# Test script for exporting uvfits

import argparse
import logging
import os
import sys
import tempfile

import numpy
from museros.apps.muser_exporter import cli_parser, muser_exporter
from museros.data_models.parameters import (
    muser_data_path,
    muser_output_path,
    muser_path,
)

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def test_export_uvfits():
    numpy.seterr(all="ignore")
    testPath = tempfile.mkdtemp(prefix="test-export-uvfits-", suffix=".tmp")

    log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_16m')}")
    data_file_name = muser_path(path="tests/test_data/test_data_16m")

    # log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_1m')}")
    # data_file_name = muser_path(path="tests/test_data/test_data_1m")

    log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_16m')}")
    data_file_name = muser_path(path="tests/test_data/test_data_1m")

    testFile = os.path.join(testPath, "test.fits")

    log.info(f"Test data file : {data_file_name}")

    export_uvfits_args = [
        "--muser",
        "MUSERI",
        "--file",
        data_file_name,
        "--output",
        testFile,
        "--cal",
        "",
        "--type",
        "fits",
        "--flag_baselines",
        "",
    ]
    parser = cli_parser()
    args = parser.parse_args(export_uvfits_args)
    muser_exporter(args)
