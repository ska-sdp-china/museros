# Test script for exporting measurement-set file

import logging
import os
import sys
import tempfile
from astropy.time import Time

import numpy
from museros.apps.muser_exporter import cli_parser, muser_exporter
from museros.data_models.parameters import (
    muser_data_path,
    muser_output_path,
    muser_path,
)

import casacore

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def test_export_ms():
    log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_16m')}")
    data_file_name = muser_path(path="tests/test_data/test_data_16m")

    # log.info(f"Muser Path: {muser_path(path = 'tests/test_data/MUSERI_SUN_W16_20230115_025953')}")
    # data_file_name = muser_path(path="tests/test_data/MUSERI_SUN_W16_20230115_025953")
    numpy.seterr(all="ignore")
    testPath = tempfile.mkdtemp(prefix="test-export-ms-", suffix=".tmp")

    testFile = os.path.join(testPath, "test.ms")

    log.info(f"Test data file : {data_file_name}")

    export_ms_args = [
        "--muser",
        "MUSERI",
        "--file",
        data_file_name,
        "--output",
        testFile,
        "--cal",
        "",
        "--type",
        "ms",
        "--flag_baselines",
        "",
    ]
    parser = cli_parser()
    args = parser.parse_args(export_ms_args)
    muser_exporter(args)

    # Open the table and examine
    ms = casacore.tables.table(testFile, ack=False)
    uvw = ms.getcol("UVW")
    ant1 = ms.getcol("ANTENNA1")
    ant2 = ms.getcol("ANTENNA2")
    vis = ms.getcol("DATA")
    weights = ms.getcol("WEIGHT_SPECTRUM")
    time = ms.getcol("TIME")
    print("time in ms:", time)
    utc = Time(time / 86400.0, format="mjd", scale="utc").datetime64
    ms2 = casacore.tables.table(os.path.join(testFile, "ANTENNA"), ack=False)
    mapper = ms2.getcol("NAME")

    assert os.path.exists(testFile)
