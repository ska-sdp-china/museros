""" Data path testing


"""

import unittest

from museros.components.utils.installation_checks import check_data_directory
from museros.data_models.parameters import muser_data_path, muser_path


class TestDataPath(unittest.TestCase):
    def test_muser_data_path(self):
        check_data_directory()


if __name__ == "__main__":
    unittest.main()
