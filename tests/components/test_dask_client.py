"""Unit tests for dask_client"""

from copy import deepcopy

import pytest
from dask.delayed import DelayedLeaf
from dask.distributed import Client, Future

from museros.components.utils.dask_client import (
    DaskClient,
    get_dask_client,
)


def _add(x, y):
    """Helper function to test execution"""
    return x + y


@pytest.fixture(scope="module", name="no_dask_client")
def no_dask_client_fxt():
    """Dask Client not using Dask"""
    client = DaskClient(use_dask=False)
    yield client
    client.close()


@pytest.fixture(scope="function", name="dask_client")
def dask_client_fxt():
    """Dask Client using Dask"""
    client = DaskClient(use_dask=True)
    yield client
    client.close()


@pytest.mark.parametrize(
    "threads_per_worker, memory_limit, " "expected_thread_num, expected_mem_limit",
    [
        (1, None, 1, 0),  # defaults
        (9, None, 9, 0),
        (1, 10e9, 1, 10e9),
        (11, 10e9, 11, 10e9),
    ],
)
def test_get_dask_client(
    threads_per_worker, memory_limit, expected_thread_num, expected_mem_limit
):
    """
    Created Dask client sets the right threads and memory limits
    to workers based on function input.
    """
    result = get_dask_client(
        threads_per_worker=threads_per_worker, memory_limit=memory_limit
    )

    assert isinstance(result, Client)
    assert len(result.scheduler_info()["workers"]) == len(result.nthreads())
    for _, worker in result.scheduler_info()["workers"].items():
        assert worker["nthreads"] == expected_thread_num
        assert worker["memory_limit"] == expected_mem_limit


def test_dask_client_no_dask_client(no_dask_client):
    """
    When Dask is not used, client is set correctly
    """
    assert no_dask_client.client is None
    assert no_dask_client.using_dask is False

    no_dask_client.client = "some-client"

    assert no_dask_client.client is None


def test_dask_client_no_dask_delayed(no_dask_client):
    """
    When Dask is not used, delayed returns input function
    """
    result = no_dask_client.delayed(_add)
    # pylint: disable-next=comparison-with-callable
    assert result == _add


def test_dask_client_no_dask_compute(no_dask_client):
    """
    When Dask is not used, compute returns value as it was passed in
    """
    value = no_dask_client.delayed(_add)(1, 2)
    result = no_dask_client.compute(value)
    assert result == 3


def test_dask_client_no_dask_run(no_dask_client):
    """
    When Dask is not used, run executes function
    """
    result = no_dask_client.run(_add, 1, 2)
    assert result == 3


def test_dask_client_init(dask_client):
    """
    When Dask is not used, client is set correctly
    """
    assert dask_client.client is None
    assert dask_client.using_dask is True


def test_dask_client_set_client(dask_client):
    """
    When Dask is used, client is set correctly.

    if set to "None", default cluster is created
    if set to a given client, that is used
    """
    tmp_client = deepcopy(dask_client)
    assert tmp_client.client is None

    tmp_client.client = None
    assert isinstance(tmp_client.client, Client)
    for _, worker in tmp_client.client.scheduler_info()["workers"].items():
        assert worker["memory_limit"] == 0
    tmp_client.close()

    client = get_dask_client(memory_limit=1e9)
    tmp_client.client = client
    assert isinstance(tmp_client.client, Client)
    for _, worker in tmp_client.client.scheduler_info()["workers"].items():
        assert worker["memory_limit"] == 1e9


def test_dask_client_delayed(dask_client):
    """
    When Dask is used, delayed returns a delayed object
    """
    result = dask_client.delayed(_add)
    assert isinstance(result, DelayedLeaf)


@pytest.mark.parametrize("sync", [True, False])
def test_dask_client_compute_no_client(sync, dask_client):
    """
    When Dask is used, but client is not set,
    the computed result will be returned no matter
    what sync is set to
    """
    assert dask_client.client is None

    value = dask_client.delayed(_add)(1, 2)
    result = dask_client.compute(value, sync=sync)
    assert result == 3


@pytest.mark.parametrize("sync, expected_type", [(True, int), (False, Future)])
def test_dask_client_compute(sync, expected_type, dask_client):
    """
    When Dask is used, compute returns value if sync is True,
    else returns a future object.
    """
    tmp_client = deepcopy(dask_client)

    # set dask client to default
    tmp_client.client = None

    value = tmp_client.delayed(_add)(1, 2)
    result = tmp_client.compute(value, sync=sync)
    assert isinstance(result, expected_type)

    tmp_client.close()


def test_dask_client_run(dask_client):
    """
    When Dask is used, run executes function on
    each worker and returns a dict of those results
    """
    tmp_client = deepcopy(dask_client)

    # set dask client to default
    tmp_client.client = None

    result = tmp_client.run(_add, 1, 2)
    for _, value in result.items():
        assert value == 3
