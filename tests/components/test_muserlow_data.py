# Data receiver for Xengine

import logging
import os
import sys

import numpy
from museros.data_models.muser_data import MuserData
from museros.data_models.parameters import (
    muser_data_path,
    muser_output_path,
    muser_path,
)

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def test_read_data():
    log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_1m_muserl')}")
    file_name = muser_path(path="tests/test_data/test_data_1m_muserl")
    log.info(f"Test data file : {file_name}")
    muser = MuserData(muser_array="muserl", file_name=file_name)
    # Read a frame
    if not muser.read_frame():
        log.error("File reading error. ")
        return False

    vis_data = numpy.zeros(
        (
            1,
            muser.n_antennas * (muser.n_antennas - 1) // 2 + muser.n_antennas,
            muser.n_channels,
            muser.n_polarizations,
        ),
        dtype=numpy.complex128,
    )

    vis_data[0, :, :, :] = muser.correlation[:, :, :]

    index = 0
    for ant1 in range(muser.n_antennas):
        for ant2 in range(ant1, muser.n_antennas):
            if ant1 == ant2:
                if ant1 == 0:
                    assert vis_data[0, index, 0, 0] == 11.453125 + 0j
                if ant1 == 39:
                    assert vis_data[0, index, 0, 0] == 7.890625 + 0j
            else:
                if ant1 == 18 and ant2 == 19:
                    assert vis_data[0, index, 10, 0] == -0.03125 - 0.078125j
                elif ant1 == 38 and ant2 == 39:
                    assert vis_data[0, index, 0, 0] == -0.046875 - 0.078125j
            index += 1
