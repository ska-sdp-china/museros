# Data receiver for Xengine

import logging
import math
import sys

from museros.components.io.xengine_muser import MuserXEngine
from museros.components.utils.installation_checks import check_data_directory
from museros.data_models.parameters import muser_data_path, muser_path

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def test_read_data_16m():
    log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_16m')}")
    data_file_name = muser_path(path="tests/test_data/test_data_16m")
    log.info(f"Test data file : {data_file_name}")
    xfile = MuserXEngine(muser_array="MUSERI")
    if not xfile.open(data_file_name):
        print("Error: No files exist.")
    assert xfile.read_frame() == True
    auto_correlation = xfile.auto_correlation
    auto_correlation_square = xfile.auto_correlation_square
    cross_correlation = xfile.cross_correlation
    # Auto correlation
    assert math.isclose(auto_correlation[0, 0], 0.0, rel_tol=0.001)
    assert math.isclose(auto_correlation[0, 0], 0.0, rel_tol=0.001)
    assert math.isclose(auto_correlation[39, 0], 1572864.046875, rel_tol=0.001)
    assert math.isclose(auto_correlation[18, 10], 786442.0, rel_tol=0.001)
    assert math.isclose(auto_correlation_square[0, 0], 6291456.001709, rel_tol=0.001)
    assert math.isclose(
        auto_correlation_square[18, 10], 52777235525632.156250, rel_tol=0.001
    )
    assert math.isclose(
        auto_correlation_square[39, 24], 105554739486720.37500, rel_tol=0.001
    )

    assert cross_correlation[18, 19, 10] == 786442 + 786442.015625j
    assert cross_correlation[38, 39, 0] == 1572864.281250 + 1572864.296875j


def test_read_data_1m():
    log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_1m')}")
    data_file_name = muser_path(path="tests/test_data/test_data_1m")
    log.info(f"Test data file : {data_file_name}")
    xfile = MuserXEngine(muser_array="MUSERI")
    if not xfile.open(data_file_name):
        print("Error: No files exist.")
    assert xfile.read_frame() == True
    auto_correlation = xfile.auto_correlation
    auto_correlation_square = xfile.auto_correlation_square
    cross_correlation = xfile.cross_correlation
    # Auto correlation
    assert auto_correlation[39, 0] == 1572864.046875
    assert auto_correlation[39, 399] == 1573263.046875
    assert auto_correlation_square[39, 399] == 105579905310726.234375
    assert cross_correlation[38, 39, 399] == 1573263.281250 + 1573263.296875j


def test_read_data_4m():
    log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_4m')}")
    data_file_name = muser_path(path="tests/test_data/test_data_4m")
    log.info(f"Test data file : {data_file_name}")
    xfile = MuserXEngine(muser_array="MUSERI")
    if not xfile.open(data_file_name):
        print("Error: No files exist.")
    assert xfile.read_frame() == True
    auto_correlation = xfile.auto_correlation
    auto_correlation_square = xfile.auto_correlation_square
    cross_correlation = xfile.cross_correlation
    # Auto correlation
    assert auto_correlation[39, 0] == 1572864.046875
    assert auto_correlation[39, 99] == 1572963.046875
    assert auto_correlation_square[39, 99] == 105559772651521.546875
    assert cross_correlation[38, 39, 99] == 1572963.281250 + 1572963.296875j
