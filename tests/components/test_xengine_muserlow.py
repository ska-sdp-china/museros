# Data receiver for Xengine

import logging
import math
import sys

# from museros.components.io.xengine_muserlow import XMuserLowEngine
from museros.components.io.xengine_muser import MuserXEngine
from museros.components.utils.installation_checks import check_data_directory
from museros.data_models.parameters import muser_data_path, muser_path

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def test_read_data_W1():
    log.info(f"Muser Path: {muser_path(path = 'tests/test_data/test_data_1m_muserl')}")
    data_file_name = muser_path(path="tests/test_data/test_data_1m_muserl")
    log.info(f"Test data file : {data_file_name}")
    xfile = MuserXEngine(muser_array="MUSERL")
    if not xfile.open(data_file_name):
        print("Error: No files exist.")
    assert xfile.read_frame() == True
    auto_correlation = xfile.auto_correlation
    # auto_correlation_square = xfile.auto_correlation_square
    cross_correlation = xfile.cross_correlation
    # Auto correlation
    assert math.isclose(auto_correlation[0, 0, 0], 11.453125, rel_tol=0.001)
    assert math.isclose(auto_correlation[0, 0, 1], 24.203125, rel_tol=0.001)
    assert math.isclose(auto_correlation[19, 103, 1], 2.890625, rel_tol=0.001)

    # R1, R6, channel:10, pol:2
    assert cross_correlation[0, 3, 10, 2] == 0.109375 - 0.125j
    assert cross_correlation[15, 99, 103, 1] == 0j

    # # Generate assert statements
    # for i in range(1, 50):
    #     for j in range(100, 101, 20):
    #         for l in [0]:
    #             for k in [0]:
    #                 print(f"assert cross_correlation[{i}, {j}, {l}, {k}] == {cross_correlation[i, j, l, k]}")

    assert cross_correlation[0, 1, 0, 0] == 0j
    assert cross_correlation[0, 1, 0, 3] == 0j
    assert cross_correlation[0, 1, 30, 0] == 0j
    assert cross_correlation[0, 1, 30, 3] == 0j
    assert cross_correlation[0, 1, 60, 0] == 0j
    assert cross_correlation[0, 1, 60, 3] == 0j
    assert cross_correlation[0, 1, 90, 0] == 0j
    assert cross_correlation[0, 1, 90, 3] == 0j
    assert cross_correlation[0, 21, 0, 0] == (-0.21875 + 0j)
    assert cross_correlation[0, 21, 0, 3] == (-0.40625 + 0j)
    assert cross_correlation[0, 21, 30, 0] == (-0.0625 - 0.078125j)
    assert cross_correlation[0, 21, 30, 3] == (-0.265625 - 0.015625j)
    assert cross_correlation[0, 21, 60, 0] == (-0.09375 + 1.546875j)
    assert cross_correlation[0, 21, 60, 3] == (-0.28125 + 0.53125j)
    assert cross_correlation[0, 21, 90, 0] == (-168.78125 + 83.65625j)
    assert cross_correlation[0, 21, 90, 3] == (11 + 3.875j)
    assert cross_correlation[0, 41, 0, 0] == 0.03125j
    assert cross_correlation[0, 41, 0, 3] == (0.109375 + 0.21875j)
    assert cross_correlation[0, 41, 30, 0] == (0.09375 - 0.3125j)
    assert cross_correlation[0, 41, 30, 3] == (0.15625 - 0.0625j)
    assert cross_correlation[0, 41, 60, 0] == (-0.046875 - 0.171875j)
    assert cross_correlation[0, 41, 60, 3] == (0.125 + 0.15625j)
    assert cross_correlation[0, 41, 90, 0] == (27.78125 + 46.609375j)
    assert cross_correlation[0, 41, 90, 3] == (-100.953125 - 47.65625j)
    assert cross_correlation[0, 61, 0, 0] == (0.40625 + 0.40625j)
    assert cross_correlation[0, 61, 0, 3] == (-2.390625 + 0.40625j)
    assert cross_correlation[0, 61, 30, 0] == (-1.390625 + 1.359375j)
    assert cross_correlation[0, 61, 30, 3] == (-1.203125 - 0.625j)
    assert cross_correlation[0, 61, 60, 0] == (-0.234375 - 0.234375j)
    assert cross_correlation[0, 61, 60, 3] == (0.046875 - 0.609375j)
    assert cross_correlation[0, 61, 90, 0] == (2.34375 + 0.75j)
    assert cross_correlation[0, 61, 90, 3] == (2.296875 - 7.796875j)
    assert cross_correlation[0, 81, 0, 0] == (-0.046875 + 0.125j)
    assert cross_correlation[0, 81, 0, 3] == (-0.015625 + 0.046875j)
    assert cross_correlation[0, 81, 30, 0] == (-0.125 + 0.046875j)
    assert cross_correlation[0, 81, 30, 3] == (-0.046875 + 0.03125j)
    assert cross_correlation[0, 81, 60, 0] == (0.171875 - 0.125j)
    assert cross_correlation[0, 81, 60, 3] == (0.015625 + 0j)
    assert cross_correlation[0, 81, 90, 0] == (-4.96875 - 0.34375j)
    assert cross_correlation[0, 81, 90, 3] == (4.796875 + 7.796875j)
    assert cross_correlation[20, 21, 0, 0] == 0j
    assert cross_correlation[20, 21, 0, 3] == 0j
    assert cross_correlation[20, 21, 30, 0] == 0j
    assert cross_correlation[20, 21, 30, 3] == 0j
    assert cross_correlation[20, 21, 60, 0] == 0j
    assert cross_correlation[20, 21, 60, 3] == 0j
    assert cross_correlation[20, 21, 90, 0] == 0j
    assert cross_correlation[20, 21, 90, 3] == 0j
    assert cross_correlation[20, 41, 0, 0] == 0j
    assert cross_correlation[20, 41, 0, 3] == 0j
    assert cross_correlation[20, 41, 30, 0] == 0j
    assert cross_correlation[20, 41, 30, 3] == 0j
    assert cross_correlation[20, 41, 60, 0] == 0j
    assert cross_correlation[20, 41, 60, 3] == 0j
    assert cross_correlation[20, 41, 90, 0] == 0j
    assert cross_correlation[20, 41, 90, 3] == 0j
    assert cross_correlation[20, 61, 0, 0] == 0j
    assert cross_correlation[20, 61, 0, 3] == 0j
    assert cross_correlation[20, 61, 30, 0] == 0j
    assert cross_correlation[20, 61, 30, 3] == 0j
    assert cross_correlation[20, 61, 60, 0] == 0j
    assert cross_correlation[20, 61, 60, 3] == 0j
    assert cross_correlation[20, 61, 90, 0] == 0j
    assert cross_correlation[20, 61, 90, 3] == 0j
    assert cross_correlation[20, 81, 0, 0] == 0j
    assert cross_correlation[20, 81, 0, 3] == 0j
    assert cross_correlation[20, 81, 30, 0] == 0j
    assert cross_correlation[20, 81, 30, 3] == 0j
    assert cross_correlation[20, 81, 60, 0] == 0j
    assert cross_correlation[20, 81, 60, 3] == 0j
    assert cross_correlation[20, 81, 90, 0] == 0j
    assert cross_correlation[20, 81, 90, 3] == 0j
    assert cross_correlation[40, 41, 0, 0] == 0.046875j
    assert cross_correlation[40, 41, 0, 3] == (-0.0625 - 0.09375j)
    assert cross_correlation[40, 41, 30, 0] == (0.0625 + 0.03125j)
    assert cross_correlation[40, 41, 30, 3] == (-0.109375 + 0.125j)
    assert cross_correlation[40, 41, 60, 0] == (-0.0625 + 0.015625j)
    assert cross_correlation[40, 41, 60, 3] == (0.09375 + 0.03125j)
    assert cross_correlation[40, 41, 90, 0] == (-20.046875 + 49.6875j)
    assert cross_correlation[40, 41, 90, 3] == (-10.71875 + 166.5j)
    assert cross_correlation[40, 61, 0, 0] == (0.015625 - 0.015625j)
    assert cross_correlation[40, 61, 0, 3] == (0.046875 + 0.078125j)
    assert cross_correlation[40, 61, 30, 0] == (0.109375 - 0.15625j)
    assert cross_correlation[40, 61, 30, 3] == (-0.109375 + 0.046875j)
    assert cross_correlation[40, 61, 60, 0] == (-0.0625 + 0.109375j)
    assert cross_correlation[40, 61, 60, 3] == (0.09375 + 0.0625j)
    assert cross_correlation[40, 61, 90, 0] == (7.765625 + 5.59375j)
    assert cross_correlation[40, 61, 90, 3] == (-8.234375 + 3.75j)
    assert cross_correlation[40, 81, 0, 0] == (0.15625 - 0.03125j)
    assert cross_correlation[40, 81, 0, 3] == 0.03125j
    assert cross_correlation[40, 81, 30, 0] == (-0.09375 - 0.015625j)
    assert cross_correlation[40, 81, 30, 3] == (-0.03125 - 0.0625j)
    assert cross_correlation[40, 81, 60, 0] == (-0.03125 + 0.078125j)
    assert cross_correlation[40, 81, 60, 3] == (0.03125 + 0.0625j)
    assert cross_correlation[40, 81, 90, 0] == (-3 - 9.3125j)
    assert cross_correlation[40, 81, 90, 3] == (-4.890625 - 12.796875j)
    assert cross_correlation[60, 61, 0, 0] == (0.96875 - 0.859375j)
    assert cross_correlation[60, 61, 0, 3] == (-0.0625 + 3.5j)
    assert cross_correlation[60, 61, 30, 0] == (0.265625 + 3.390625j)
    assert cross_correlation[60, 61, 30, 3] == (-1.515625 - 1.03125j)
    assert cross_correlation[60, 61, 60, 0] == (-0.78125 - 0.640625j)
    assert cross_correlation[60, 61, 60, 3] == (1.21875 - 0.703125j)
    assert cross_correlation[60, 61, 90, 0] == (1.15625 - 1.421875j)
    assert cross_correlation[60, 61, 90, 3] == (-2.109375 + 4.359375j)
    assert cross_correlation[60, 81, 0, 0] == (-0.265625 - 0.078125j)
    assert cross_correlation[60, 81, 0, 3] == (0.09375 + 0j)
    assert cross_correlation[60, 81, 30, 0] == (-0.078125 - 0.140625j)
    assert cross_correlation[60, 81, 30, 3] == 0.03125j
    assert cross_correlation[60, 81, 60, 0] == (0.046875 - 0.34375j)
    assert cross_correlation[60, 81, 60, 3] == (0.046875 + 0.0625j)
    assert cross_correlation[60, 81, 90, 0] == (-1.546875 - 3.34375j)
    assert cross_correlation[60, 81, 90, 3] == (-0.3125 - 4.3125j)
    assert cross_correlation[80, 81, 0, 0] == (-0.046875 - 0.09375j)
    assert cross_correlation[80, 81, 0, 3] == (-0.03125 + 0.015625j)
    assert cross_correlation[80, 81, 30, 0] == (-0.09375 - 0.046875j)
    assert cross_correlation[80, 81, 30, 3] == (-0.03125 + 0j)
    assert cross_correlation[80, 81, 60, 0] == (0.453125 - 0.359375j)
    assert cross_correlation[80, 81, 60, 3] == (-0.015625 + 0.03125j)
    assert cross_correlation[80, 81, 90, 0] == (9.25 + 41.515625j)
    assert cross_correlation[80, 81, 90, 3] == (-17.53125 + 2.3125j)
    assert cross_correlation[0, 100, 0, 0] == (0.5625 + 0.921875j)
    assert cross_correlation[0, 100, 30, 0] == (-0.796875 + 0.953125j)
    assert cross_correlation[0, 100, 60, 0] == (5.0625 - 0.1875j)
    assert cross_correlation[0, 100, 90, 0] == (-390.109375 - 355.859375j)

    assert cross_correlation[1, 100, 0, 0] == 0j
    assert cross_correlation[2, 100, 0, 0] == (0.625 + 2.984375j)
    assert cross_correlation[3, 100, 0, 0] == (-1.09375 + 1.78125j)
    assert cross_correlation[4, 100, 0, 0] == (1.734375 - 1.6875j)
    assert cross_correlation[5, 100, 0, 0] == (1.546875 - 0.203125j)
    assert cross_correlation[6, 100, 0, 0] == (0.203125 - 1.21875j)
    assert cross_correlation[7, 100, 0, 0] == (1.078125 + 0.625j)
    assert cross_correlation[8, 100, 0, 0] == (0.3125 - 0.9375j)
    assert cross_correlation[9, 100, 0, 0] == (-0.125 - 0.375j)
    assert cross_correlation[10, 100, 0, 0] == (-0.8125 + 1.796875j)
    assert cross_correlation[11, 100, 0, 0] == 0j
    assert cross_correlation[12, 100, 0, 0] == (1.21875 - 0.9375j)
    assert cross_correlation[13, 100, 0, 0] == (4.890625 + 2.234375j)
    assert cross_correlation[14, 100, 0, 0] == (0.234375 + 0.578125j)
    assert cross_correlation[15, 100, 0, 0] == 0j
    assert cross_correlation[16, 100, 0, 0] == (-0.15625 - 0.046875j)
    assert cross_correlation[17, 100, 0, 0] == (-1.5625 - 0.859375j)
    assert cross_correlation[18, 100, 0, 0] == (0.15625 + 0.1875j)
    assert cross_correlation[19, 100, 0, 0] == (0.875 + 0.640625j)
    assert cross_correlation[20, 100, 0, 0] == 0j
    assert cross_correlation[21, 100, 0, 0] == (0.421875 + 16.921875j)
    assert cross_correlation[22, 100, 0, 0] == (18.046875 + 8j)
    assert cross_correlation[23, 100, 0, 0] == (-19.28125 + 0.53125j)
    assert cross_correlation[24, 100, 0, 0] == (-0.40625 + 0.703125j)
    assert cross_correlation[25, 100, 0, 0] == (1.375 - 0.109375j)
    assert cross_correlation[26, 100, 0, 0] == (-2.25 - 1.59375j)
    assert cross_correlation[27, 100, 0, 0] == (-2.625 + 4.4375j)
    assert cross_correlation[28, 100, 0, 0] == (0.640625 - 1.4375j)
    assert cross_correlation[29, 100, 0, 0] == 0j
    assert cross_correlation[30, 100, 0, 0] == (0.546875 + 2j)
    assert cross_correlation[31, 100, 0, 0] == (2.078125 - 5.46875j)
    assert cross_correlation[32, 100, 0, 0] == (-0.375 + 0.125j)
    assert cross_correlation[33, 100, 0, 0] == (0.046875 - 3.40625j)
    assert cross_correlation[34, 100, 0, 0] == (-0.21875 - 0.328125j)
    assert cross_correlation[35, 100, 0, 0] == (-1.34375 - 0.265625j)
    assert cross_correlation[36, 100, 0, 0] == (-0.734375 - 1.75j)
    assert cross_correlation[37, 100, 0, 0] == (0.34375 - 1.375j)
    assert cross_correlation[38, 100, 0, 0] == (7.359375 - 0.296875j)
    assert cross_correlation[39, 100, 0, 0] == (-1.890625 + 1.9375j)
    assert cross_correlation[40, 100, 0, 0] == (1.890625 - 2.078125j)
    assert cross_correlation[41, 100, 0, 0] == (-0.65625 - 1.453125j)
    assert cross_correlation[42, 100, 0, 0] == (-1.046875 + 1.578125j)
    assert cross_correlation[43, 100, 0, 0] == (-0.34375 - 0.15625j)
    assert cross_correlation[44, 100, 0, 0] == (-0.171875 + 1.078125j)
    assert cross_correlation[45, 100, 0, 0] == (4.828125 + 2.421875j)
    assert cross_correlation[46, 100, 0, 0] == (-3.296875 + 1.4375j)
    assert cross_correlation[47, 100, 0, 0] == (-0.15625 - 2.359375j)
    assert cross_correlation[48, 100, 0, 0] == (5.34375 - 4.84375j)
    assert cross_correlation[49, 100, 0, 0] == (-0.4375 - 1j)
