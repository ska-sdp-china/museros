# simple makefile to simplify repetitive build env management tasks under posix

PYTHON ?= python3
PYLINT ?= pylint
MAKE_DBG ?= ""
TESTS ?= tests/
FLAKE ?= flake8
NAME = museros
IMG ?= $(NAME)
TAG ?= ubuntu18.04
CURRENT_DIR = $(shell pwd)
JUPYTER_PASSWORD ?= changeme

CRED=\033[0;31m
CBLUE=\033[0;34m
CEND=\033[0m
LINE:=$(shell printf '=%.0s' {1..70})

# RASCIL2 data directory usualy found in ./data

# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

all: test lint docs

.PHONY: all test lint docs

lint:
# outputs linting.xml
	pylint --exit-zero --extension-pkg-whitelist=numpy --ignored-classes=astropy.units,astropy.constants,HDUList --output-format=pylint_junit.JUnitReporter:linting.xml src
	pylint --exit-zero --extension-pkg-whitelist=numpy --ignored-classes=astropy.units,astropy.constants,HDUList --output-format=parseable src
	black --check .

docs:  ## build docs
# Outputs docs/build/html
	$(MAKE) -C docs/src html

test:
	HOME=`pwd` py.test -n 4 \
	tests/apps tests/data_models tests/components --verbose \
	--cov=src \
	--junitxml unit-tests-other.xml \
	--pylint --pylint-error-types=EF --durations=30
	coverage html -d coverage
