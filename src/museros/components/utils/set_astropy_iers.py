import os

from astropy.config import get_config_dir
from astropy.utils.iers import conf as iers_conf

# 自定义IERS数据下载地址（替换为你的实际URL）
CUSTOM_IERS_URL = (
    "https://datacenter.iers.org/products/eop/rapid/standard/finals2000A.all"
)


def configure_iers_settings():
    """配置并保存IERS设置"""
    # 1. 设置自定义下载地址
    iers_conf.iers_auto_url = CUSTOM_IERS_URL

    # 2. 保存配置到文件
    config_dir = get_config_dir()
    config_path = os.path.join(config_dir, "astropy.cfg")

    # 确保配置目录存在
    os.makedirs(config_dir, exist_ok=True)

    # 写入配置项
    with open(config_path, "w") as f:
        f.write("[utils.iers]\n")
        f.write(f"iers_auto_url = {CUSTOM_IERS_URL}\n")

    print(f"Current IERS URL: {iers_conf.iers_auto_url}")
    print(f"The config has been saved to : {config_path}")
