import os
import platform
import sys

import psutil


def check_cpu_count():
    if platform.system() != "Linux":
        print("This script can only run on Linux.")
        sys.exit(1)

    cpu_count = os.cpu_count()
    print(f"Number of CPUs: {cpu_count}")


def check_hardware_resources():
    # 获取CPU使用率
    cpu_usage = psutil.cpu_percent(interval=1)
    print(f"CPU Usage: {cpu_usage}%")

    # 获取内存使用情况
    memory_info = psutil.virtual_memory()
    print(f"Total Memory: {memory_info.total / (1024 ** 3):.2f} GB")
    print(f"Available Memory: {memory_info.available / (1024 ** 3):.2f} GB")
    print(f"Used Memory: {memory_info.used / (1024 ** 3):.2f} GB")
    print(f"Memory Usage: {memory_info.percent}%")

    # 获取磁盘使用情况
    disk_info = psutil.disk_usage("/")
    print(f"Total Disk Space: {disk_info.total / (1024 ** 3):.2f} GB")
    print(f"Used Disk Space: {disk_info.used / (1024 ** 3):.2f} GB")
    print(f"Free Disk Space: {disk_info.free / (1024 ** 3):.2f} GB")
    print(f"Disk Usage: {disk_info.percent}%")


if __name__ == "__main__":
    check_hardware_resources()
    check_cpu_count()
