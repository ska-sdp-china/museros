"""
Wrap das
k such that with the same code Dask.delayed
can be replaced by immediate calculation
"""

import logging
import os
import time

import astropy.constants
import astropy.units as u
import dask.config
import erfa  # direct dependency of astropy
from astropy.coordinates import SkyCoord
from dask import config, delayed
from dask.distributed import Client

log = logging.getLogger("imagingqa-logger")


def get_dask_client(
    timeout=30,
    threads_per_worker=1,
    memory_limit=None,
):
    """
    Get a Dask.distributed Client

    The environment variable DASK_SCHEDULER is interpreted
    as pointing to the Dask distributed scheduler and a client
    using that scheduler is returned.
    Otherwise, a local client is created.

    :param timeout: Time out for creation (30s)
    :param threads_per_worker: How many threads to user per worker (1)
    :param memory_limit: Memory limit per worker (bytes e.g. 8e9) (None)
    :return: Dask client
    """
    scheduler = os.getenv("DASK_SCHEDULER")

    if scheduler is not None:
        log.info("Creating Dask Client using externally defined scheduler")
        client = Client(scheduler, timeout=timeout)

    else:
        log.info("Creating Dask.distributed Client")
        client = Client(
            threads_per_worker=threads_per_worker,
            memory_limit=memory_limit,
            local_directory=".",
        )

    log.info("Dask dashboard link: %s", client.dashboard_link)

    return client


class DaskClient:
    """
    Initialise Dask framework

    :param use_dask: Use dask (True)
    """

    def __init__(self, use_dask=True):
        self._using_dask = use_dask
        self._client = None

        # Initialize astropy to avoid threading problems
        if self._using_dask:
            assert erfa.s2c(0 * u.rad, 0 * u.rad)[0] == 1.0
            # pylint: disable-next=no-member
            assert astropy.constants.c.unit == "m/s"
            assert isinstance(
                SkyCoord(0.0 * u.rad, 0.0 * u.rad, frame="icrs").to_string(),
                str,
            )
            assert (
                SkyCoord(0.0 * u.rad, 0.0 * u.rad, frame="icrs").skyoffset_frame().name
                == "skyoffseticrs"
            )

    @property
    def client(self):
        """Client being used"""
        return self._client

    @property
    def using_dask(self):
        """Is dask being used?"""
        return self._using_dask

    @client.setter
    def client(self, client):
        """
        Set the Dask client to be used

        If you want to customise the Client or use an externally
        defined Scheduler use get_dask_client and pass it in.

        :param client: If None and use_dask is True, a client
            will be created otherwise the client is None
        """
        # We need this so that xarray knows which scheduler to use
        if not self.using_dask:
            log.info("Dask is not used, not setting Dask client.")
            self._client = None

        else:
            config.set(scheduler="distributed")

            if isinstance(self._client, Client):
                log.debug("Removing existing client.")
                self._client.close()

            self._client = client or get_dask_client()
            self._client.profile()
            self._client.get_task_stream()

            log.debug("Defined Dask distributed client.")

    def delayed(self, func, *args, **kwargs):
        """
        Wrap for immediate or deferred execution.
        Passes through if dask is not being used.
        """
        if self.using_dask:
            return delayed(func, *args, **kwargs)
        return func

    def compute(self, value, sync=False):
        """
        Get the actual value

        If not using dask then this returns the value directly
        since it already is computed.

        If using dask and sync=True then this waits and
        returns the actual wait.

        If using dask and sync=False then this returns a future,
        on which you will need to call .result()
        """
        if self.using_dask:
            start = time.time()
            if self.client is None:
                return value.compute()

            future = self.client.compute(value, sync=sync)

            duration = time.time() - start
            msg = f"Execution using Dask took {duration} seconds."
            log.debug(msg)

            return future

        return value

    def run(self, func, *args, **kwargs):
        """Run a function on the client"""
        if self.using_dask:
            if self.client is not None:
                return self.client.run(func, *args, **kwargs)
            return func(*args, **kwargs)
        return func(*args, **kwargs)

    def close(self):
        """Close the client"""
        if self.using_dask and isinstance(self._client, Client):
            if self._client.cluster is not None:
                self._client.cluster.close()
            self._client.close()
            self._client = None
            log.debug("Closed down Dask Client.")

    def info(self):
        """Get information about the client"""
        if self.using_dask:
            return self.client.scheduler_info()
        return None
