# Data receiver for Xengine

import copy
import logging
from datetime import datetime
from os import R_OK, access
from os.path import isfile
from struct import unpack

import numpy

log = logging.getLogger("muser-logger")
log.setLevel(logging.DEBUG)

N_BLOCKS = 25
ANTS_PER_BLOCK = 8
AUTO_SIZE_PER_ANT = 3
CROSS_SIZE_PER_PAIR = 6
INTRA_CROSS_PAIRS_PER_BLOCK = 24


class XEngineMuserLowFrameHeader:
    """
    Frame of XEngine Used in MUSER
    """

    def __init__(self):
        """
        Constructor of XengineFrameHeader
        """
        self.epoch = None
        self.year = None
        self.month = None
        self.day = None
        self.hour = None
        self.minute = None
        self.second = None
        self.in_second = None
        self.freq_band = None

        self.gen_bit = None
        self.phase_switch = False
        self.delay_switch = False
        self.valid = True

        self.obs_bit = None
        self.source = None
        self.bandwidth = None
        self.polarization = None
        self.number_of_frames = None

    def __repr__(self):
        return f'Frame:("{self.epoch}","{self.in_second}","{self.freq_band}")'

    def __str__(self):
        return f"Frame: {self.epoch} - In Seconds:{self.in_second:X}, Frequency:{self.freq_band:X}, Bandwidth:{self.bandwidth}"


class XEngineMuserLowFrame(object):
    def __init__(self, xengine_file=None):
        """Constructor of XengineFrame

        :param muser_array: Muser array ID, defaults to "MUSERL"
        """

        # File handler
        self.xengine_file = xengine_file
        # Frame header
        self.frame_head = XEngineMuserLowFrameHeader()

        # The number of channels in different bandwidths
        self._number_of_channels_dict = {1: 104, 4: 26, 8: 13}

        self._channel_dict = {
            0x33: 30,  # 30~80
            0x77: 100,  # 100~200
            0xBB: 200,  # 200~300
            0xCC: 300,  # 300~400
            0xA3: 30,  # Loop Mode
            0xA7: 100,
            0xAB: 200,
            0xAC: 300,
        }

        self._band_dict = {
            0x33: 1,  # 30~80
            0x77: 2,  # 100~200
            0xBB: 3,  # 200~300
            0xCC: 4,  # 300~400
            0xA3: 1,  # Loop Mode
            0xA7: 2,
            0xAB: 3,
            0xAC: 4,
        }

        self._number_of_bands_dict = {
            0x33: 1,  # Fix Mode, one band
            0x77: 1,
            0xBB: 1,
            0xCC: 1,
            0xA3: 4,  # Loop Mode, 4 bands
            0xA7: 4,
            0xAB: 4,
            0xAC: 4,
        }

        self._source_dict = {
            0xAA: "SUN",
            0xA1: "NOISE",
            0xA2: "OTHER",
            0xA3: "SELF-DEFINE",
        }

        self._start_freq = {
            0: [14648437.5, 16113281.25, 18066406.25],
            1: [99648437.5, 101113281.25, 103066406.25],
            2: [199648437.5, 201113281.25, 203066406.25],
            3: [299648437.5, 301113281.25, 303066406.25],
        }

        self._channel_width = {0: 976562.5, 4: 3906250.0, 8: 7812500.0}

        self._polarization_dict = {0: "XX", 1: "XY", 2: "YX", 3: "YY"}

        # Mode 1 - H 18b L6b, 4:20b4b 8:21b3b
        self._fixed_float = {1: 64, 4: 16, 8: 8}
        self.n_antennas = 101
        self.n_polarizations = 4
        self.polarization = None

        self.n_channels = None
        self.n_baselines = None
        self.auto_correlation = None
        self.auto_correlation_square = None
        self.cross_correlation = None
        self._init_data = None
        self.n_bands = None
        self.frac_off = 0
        self.s_broadband = 0
        self.dr_output_antennas = 101
        self.block_cros = [
            [1, 2],
            [1, 3],
            [1, 4],
            [1, 5],
            [1, 6],
            [1, 7],
            [1, 8],
            [1, 9],
            [1, 10],
            [1, 11],
            [1, 12],
            [1, 13],
            [1, 14],
            [1, 15],
            [1, 16],
            [1, 17],
            [1, 18],
            [1, 19],
            [1, 20],
            [1, 21],
            [1, 22],
            [1, 23],
            [1, 24],
            [1, 25],
            [2, 3],
            [2, 4],
            [2, 5],
            [2, 6],
            [2, 7],
            [2, 8],
            [2, 9],
            [2, 10],
            [2, 11],
            [2, 12],
            [2, 13],
            [2, 14],
            [2, 15],
            [2, 16],
            [2, 17],
            [2, 18],
            [2, 19],
            [2, 20],
            [2, 21],
            [2, 22],
            [2, 23],
            [2, 24],
            [2, 25],
            [23, 24],
            [23, 25],
            [24, 25],
            [3, 4],
            [3, 5],
            [3, 6],
            [3, 7],
            [3, 8],
            [3, 9],
            [3, 10],
            [3, 11],
            [3, 12],
            [3, 13],
            [3, 14],
            [3, 15],
            [3, 16],
            [3, 17],
            [3, 18],
            [3, 19],
            [3, 20],
            [3, 21],
            [3, 22],
            [3, 23],
            [3, 24],
            [3, 25],
            [4, 5],
            [4, 6],
            [4, 7],
            [4, 8],
            [4, 9],
            [4, 10],
            [4, 11],
            [4, 12],
            [4, 13],
            [4, 14],
            [4, 15],
            [4, 16],
            [4, 17],
            [4, 18],
            [4, 19],
            [4, 20],
            [4, 21],
            [4, 22],
            [4, 23],
            [4, 24],
            [4, 25],
            [21, 22],
            [21, 23],
            [21, 24],
            [21, 25],
            [22, 23],
            [22, 24],
            [22, 25],
            [5, 6],
            [5, 7],
            [5, 8],
            [5, 9],
            [5, 10],
            [5, 11],
            [5, 12],
            [5, 13],
            [5, 14],
            [5, 15],
            [5, 16],
            [5, 17],
            [5, 18],
            [5, 19],
            [5, 20],
            [5, 21],
            [5, 22],
            [5, 23],
            [5, 24],
            [5, 25],
            [6, 7],
            [6, 8],
            [6, 9],
            [6, 10],
            [6, 11],
            [6, 12],
            [6, 13],
            [6, 14],
            [6, 15],
            [6, 16],
            [6, 17],
            [6, 18],
            [6, 19],
            [6, 20],
            [6, 21],
            [6, 22],
            [6, 23],
            [6, 24],
            [6, 25],
            [19, 20],
            [19, 21],
            [19, 22],
            [19, 23],
            [19, 24],
            [19, 25],
            [20, 21],
            [20, 22],
            [20, 23],
            [20, 24],
            [20, 25],
            [7, 8],
            [7, 9],
            [7, 10],
            [7, 11],
            [7, 12],
            [7, 13],
            [7, 14],
            [7, 15],
            [7, 16],
            [7, 17],
            [7, 18],
            [7, 19],
            [7, 20],
            [7, 21],
            [7, 22],
            [7, 23],
            [7, 24],
            [7, 25],
            [8, 9],
            [8, 10],
            [8, 11],
            [8, 12],
            [8, 13],
            [8, 14],
            [8, 15],
            [8, 16],
            [8, 17],
            [8, 18],
            [8, 19],
            [8, 20],
            [8, 21],
            [8, 22],
            [8, 23],
            [8, 24],
            [8, 25],
            [17, 18],
            [17, 19],
            [17, 20],
            [17, 21],
            [17, 22],
            [17, 23],
            [17, 24],
            [17, 25],
            [18, 19],
            [18, 20],
            [18, 21],
            [18, 22],
            [18, 23],
            [18, 24],
            [18, 25],
            [9, 10],
            [9, 11],
            [9, 12],
            [9, 13],
            [9, 14],
            [9, 15],
            [9, 16],
            [9, 17],
            [9, 18],
            [9, 19],
            [9, 20],
            [9, 21],
            [9, 22],
            [9, 23],
            [9, 24],
            [9, 25],
            [10, 11],
            [10, 12],
            [10, 13],
            [10, 14],
            [10, 15],
            [10, 16],
            [10, 17],
            [10, 18],
            [10, 19],
            [10, 20],
            [10, 21],
            [10, 22],
            [10, 23],
            [10, 24],
            [10, 25],
            [15, 16],
            [15, 17],
            [15, 18],
            [15, 19],
            [15, 20],
            [15, 21],
            [15, 22],
            [15, 23],
            [15, 24],
            [15, 25],
            [16, 17],
            [16, 18],
            [16, 19],
            [16, 20],
            [16, 21],
            [16, 22],
            [16, 23],
            [16, 24],
            [16, 25],
            [11, 12],
            [11, 13],
            [11, 14],
            [11, 15],
            [11, 16],
            [11, 17],
            [11, 18],
            [11, 19],
            [11, 20],
            [11, 21],
            [11, 22],
            [11, 23],
            [11, 24],
            [11, 25],
            [12, 13],
            [12, 14],
            [12, 15],
            [12, 16],
            [12, 17],
            [12, 18],
            [12, 19],
            [12, 20],
            [12, 21],
            [12, 22],
            [12, 23],
            [12, 24],
            [12, 25],
            [13, 14],
            [13, 15],
            [13, 16],
            [13, 17],
            [13, 18],
            [13, 19],
            [13, 20],
            [13, 21],
            [13, 22],
            [13, 23],
            [13, 24],
            [13, 25],
            [14, 15],
            [14, 16],
            [14, 17],
            [14, 18],
            [14, 19],
            [14, 20],
            [14, 21],
            [14, 22],
            [14, 23],
            [14, 24],
            [14, 25],
        ]

        # Current index is only for 1M model
        self.init_index()

    def __del__(self):
        if self.xengine_file is not None:
            self.xengine_file.close()

    @property
    def mode(self):
        """Observation mode

        :return: mode (LOOP/FIXED)
        :rtype: str
        """
        if self.frame_head.freq_band in [0xA3, 0xA7, 0xAB, 0xAC]:
            return "LOOP"
        else:
            return "FIXED"

    @property
    def band(self):
        """Get current band (1-4)

        :return current band
        """
        return self._band_dict[self.frame_head.freq_band]

    @property
    def date_time(self):
        """Observational date and time

        :return: value(datetime)
        """
        value = datetime(
            self.frame_head.year,
            self.frame_head.month,
            self.frame_head.day,
            self.frame_head.hour,
            self.frame_head.minute,
            self.frame_head.second,
            self.frame_head.in_second,
        )
        return value

    @property
    def n_frames(self):
        """Get the number of frames (period cycle) in the specified file

        :return: n_frames
        """
        return self.frame_head.number_of_frames

    @property
    def channel_bandwidth(self):
        """Get channel width

        :return channel_bandwidth
        """
        return self.frame_head.bandwidth * 125 / 128 * 1e6

    @property
    def n_full_frames(self):
        """The number of frames in a FULL frame

        :return: the number of frames
        """
        return self.n_bands

    @property
    def bandwidth(self):
        """Bandwidth in the channel (1/4/8M)

        :return: channelbandwidth
        """
        return self.frame_head.bandwidth

    @property
    def read_frame_head(self):
        """Read XEngine Frame

        :return: Reading result (True/False)
        """
        if self.xengine_file is None:
            return False
        log.debug(
            "Xengine-MuserLow: File pointer after header %d", self.xengine_file.tell()
        )

        # Read 24 bytes at once
        data = self.xengine_file.read(24)
        if len(data) < 24:
            log.error("Xengine-MuserLow: Not enough data to read frame head")
            return False

        # Unpack the data
        (
            self.epoch,
            time_microsecond,
            _,
            self.frame_head.freq_band,
            self.frame_head.gen_bit,
            self.frame_head.obs_bit,
            self.frame_head.bandwidth,
            _,
            _,
            self.frame_head.number_of_frames,
        ) = unpack("<QIBBBBBBHI", data)

        log.info(
            "Xengine-MuserLow: Observation time (seconds): %s %s",
            str(self.epoch),
            str(self.frame_head.number_of_frames),
        )
        try:
            obs_date_time = datetime.fromtimestamp(self.epoch)
        except:
            log.error(
                "Xengine-MuserLow: Time stamp conversion error: %s", obs_date_time
            )

        self.frame_head.year = obs_date_time.year
        self.frame_head.month = obs_date_time.month
        self.frame_head.day = obs_date_time.day
        self.frame_head.hour = obs_date_time.hour
        self.frame_head.minute = obs_date_time.minute
        self.frame_head.second = obs_date_time.second

        self.frame_head.in_second = int(time_microsecond * 4 / 1e3)

        # Get fixed offset for float
        self.frac_off = self._fixed_float[self.frame_head.bandwidth]
        # 12668032 数据来源  index_v3.pdf文件->单个积分周期内各频点之间的排列与单个频点内的数据格式
        self.frame_size = int((12668032 / self.frame_head.bandwidth + 24))

        if self._init_data is None:
            # Frame data
            self.n_channels = self._number_of_channels_dict[self.frame_head.bandwidth]

            self.n_baselines = (
                self.n_antennas * (self.n_antennas - 1) // 2 + self.n_antennas
            )
            self.auto_correlation = numpy.ndarray(
                (self.n_antennas, self.n_channels, self.n_polarizations // 2),
                dtype=numpy.float64,
            )
            self.cross_correlation = numpy.ndarray(
                (
                    self.n_antennas,
                    self.n_antennas,
                    self.n_channels,
                    self.n_polarizations,
                ),
                dtype=numpy.complex128,
            )
            self.n_bands = self._number_of_bands_dict[self.frame_head.freq_band]
            self._init_data = True

        log.debug(f"Xengine: File pointer after header {self.xengine_file.tell()}")

        return True

    def get_info(self):
        """
        Get information of the file
        """
        log.debug(f"Xengine: Get current file information")
        if not self.read_frame_head:
            log.error("Xengine: Get frame header error.")
            return False
        # Move the file pointer to the beginning
        self.xengine_file.seek(0)
        log.debug(
            f"Xengine: frame info: {self.date_time}:{self.bandwidth}:{self.n_frames}:{self.n_channels}"
        )
        self.frame_number = self.n_bands

        return True

    def read_one_channel_from_frame_v2(self, channel):
        """
        Read Auto-Correlation and Cross-Correlation data for a specific channel using vectorized NumPy operations,
        reading data block-by-block with corrected cross-correlation indexing.


        :param channel: Channel number to read (0 to n_channels-1)
        :return: Reading result (True/False)
        """
        import numpy as np

        N_BLOCKS = 25
        ANTS_PER_BLOCK = 8
        AUTO_SIZE_PER_ANT = 3
        CROSS_SIZE_PER_PAIR = 6
        INTRA_CROSS_PAIRS_PER_BLOCK = 24

        block_size = (
            ANTS_PER_BLOCK * AUTO_SIZE_PER_ANT
            + INTRA_CROSS_PAIRS_PER_BLOCK * CROSS_SIZE_PER_PAIR
        )  # 168 bytes
        last_auto_size = 2 * AUTO_SIZE_PER_ANT  # 6 bytes
        padding_size = 2
        cross_block_pairs = (
            len(self.block_cros) * ANTS_PER_BLOCK * ANTS_PER_BLOCK
        )  # 19200 pairs
        cross_block_size = cross_block_pairs * CROSS_SIZE_PER_PAIR  # 115200 bytes
        r200_pairs = N_BLOCKS * ANTS_PER_BLOCK * 2  # 400 pairs
        r200_size = r200_pairs * CROSS_SIZE_PER_PAIR  # 2400 bytes

        total_size = (
            block_size * N_BLOCKS
            + last_auto_size
            + padding_size
            + cross_block_size
            + r200_size
        )  # 121808 bytes

        raw_data = self.xengine_file.read(total_size)
        if len(raw_data) < total_size:
            log.error(
                f"Not enough data to read frame for channel {channel}, expected {total_size}, got {len(raw_data)}"
            )
            return False

        data_array = numpy.frombuffer(raw_data, dtype=numpy.uint8)

        def process_3byte_to_float(data_3byte):
            reshaped = data_3byte.reshape(-1, 3)
            sign_mask = (reshaped[:, 0] >= 0x80).astype(numpy.uint8)
            padded = numpy.zeros((len(reshaped), 4), dtype=numpy.uint8)
            padded[:, 0] = sign_mask * 0xFF
            padded[:, 1:] = reshaped
            return padded.ravel().view(">i4").astype(numpy.float64) / self.frac_off

        offset = 0
        # 1. 处理块内自相关和交叉相关（25个块）
        for block_i in range(N_BLOCKS):
            block_data = data_array[offset : offset + block_size]

            # 自相关：8个天线
            auto_data = block_data[: ANTS_PER_BLOCK * AUTO_SIZE_PER_ANT]  # 24 bytes
            auto_values = process_3byte_to_float(auto_data)  # 8 values
            ant_indices = numpy.arange(
                block_i * ANTS_PER_BLOCK, (block_i + 1) * ANTS_PER_BLOCK
            )
            pol_indices = ant_indices % 2
            self.auto_correlation[ant_indices // 2, channel, pol_indices] = auto_values

            # 交叉相关：24对
            cross_data = block_data[ANTS_PER_BLOCK * AUTO_SIZE_PER_ANT :]  # 144 bytes
            cross_values = process_3byte_to_float(cross_data).reshape(-1, 2)
            cross_complex = cross_values[:, 0] + 1j * cross_values[:, 1]

            r0_block = []
            r1_block = []
            pol_block = []
            for r_head in range(ANTS_PER_BLOCK):
                for r_rear in range(ANTS_PER_BLOCK):
                    r0 = block_i * ANTS_PER_BLOCK + r_head
                    r1 = block_i * ANTS_PER_BLOCK + r_rear
                    if r1 <= r0 or (r1 % 2 != 0 and (r0 + 1) == r1):
                        continue
                    r0_block.append(r0)
                    r1_block.append(r1)
                    pol_block.append(r0 % 2 * 2 + r1 % 2)

            r0_indices = numpy.array(r0_block)
            r1_indices = numpy.array(r1_block)
            pol_indices = numpy.array(pol_block)
            self.cross_correlation[
                r0_indices // 2, r1_indices // 2, channel, pol_indices
            ] = cross_complex

            offset += block_size

        # 2. 处理最后的两个自相关 (R200R200, R201R201)
        last_auto_data = data_array[offset : offset + last_auto_size]
        last_auto_values = process_3byte_to_float(last_auto_data)
        self.auto_correlation[100, channel, 0] = last_auto_values[0]
        self.auto_correlation[100, channel, 1] = last_auto_values[1]
        offset += last_auto_size

        # 3. 处理2字节填充
        offset += padding_size

        # 4. 处理块间交叉相关
        cross_block_data = data_array[offset : offset + cross_block_size]
        cross_block_values = process_3byte_to_float(cross_block_data).reshape(-1, 2)
        cross_block_complex = cross_block_values[:, 0] + 1j * cross_block_values[:, 1]

        self.r0_cross = []
        self.r1_cross = []
        self.pol_cross = []
        for i in range(len(self.block_cros)):
            block0 = (self.block_cros[i][0] - 1) * ANTS_PER_BLOCK
            block1 = (self.block_cros[i][1] - 1) * ANTS_PER_BLOCK
            for r_head in range(ANTS_PER_BLOCK):
                for r_rear in range(ANTS_PER_BLOCK):
                    r0 = block0 + r_head
                    r1 = block1 + r_rear
                    self.r0_cross.append(r0)
                    self.r1_cross.append(r1)
                    self.pol_cross.append(r0 % 2 * 2 + r1 % 2)

        r0_indices = numpy.array(self.r0_cross)
        r1_indices = numpy.array(self.r1_cross)
        pol_indices = numpy.array(self.pol_cross)
        self.cross_correlation[
            r0_indices // 2, r1_indices // 2, channel, pol_indices
        ] = cross_block_complex
        offset += cross_block_size

        # 5. 处理R200/R201与其他点的交叉相关
        r200_data = data_array[offset : offset + r200_size]
        r200_values = process_3byte_to_float(r200_data).reshape(-1, 2)
        r200_complex = r200_values[:, 0] + 1j * r200_values[:, 1]

        self.r0_r200 = []
        self.r1_r200 = []
        self.pol_r200 = []
        for block_i in range(N_BLOCKS):
            for r26 in range(200, 202):
                for half in range(ANTS_PER_BLOCK):
                    r0 = block_i * ANTS_PER_BLOCK + half
                    r1 = r26
                    pol = r0 % 2 + r1 % 2 * 2
                    self.r0_r200.append(r0)
                    self.r1_r200.append(r1)
                    self.pol_r200.append(pol)

        r0_indices = numpy.array(self.r0_r200)
        r1_indices = numpy.array(self.r1_r200)
        pol_indices = numpy.array(self.pol_r200)
        self.cross_correlation[
            r0_indices // 2, r1_indices // 2, channel, pol_indices
        ] = r200_complex

        return True

    def read_one_channel_from_frame_v1(self, channel):
        """
        Read Auto-Correlation From Frame type 1 (P0-P6)

        :return: Reading result (True/False)
        """

        # 1:R0R0,R1R1,R2R2....R200R200,R201R201   相同块间计算
        for block_i in range(0, 25, 1):
            # Read Auto-correlation
            for i in range(0, 8, 1):
                r0 = block_i * 8 + i
                r1 = block_i * 8 + i
                self.polarization = i % 2

                data_re = self.xengine_file.read(3)
                data_re = (
                    int.from_bytes(data_re, byteorder="big", signed=True)
                    / self.frac_off
                )
                # data_re = (
                #     data_re // self.frac_off + (data_re % self.frac_off) / self.frac_off
                # )
                self.auto_correlation[r0 // 2][channel][i % 2] = data_re

            log.debug(f"The first loop - {r0}-{r1}:{data_re}")

            for r_head in range(0, 8, 1):
                for r_rear in range(0, 8, 1):
                    r0 = block_i * 8 + r_head
                    r1 = block_i * 8 + r_rear
                    if r1 <= r0:
                        continue
                    if r1 % 2 != 0 and (r0 + 1) == r1:
                        continue

                    self.polarization = r0 % 2 * 2 + r1 % 2

                    data_re = self.xengine_file.read(3)
                    data_re = (
                        int.from_bytes(data_re, byteorder="big", signed=True)
                        / self.frac_off
                    )

                    data_im = self.xengine_file.read(3)
                    data_im = (
                        int.from_bytes(data_im, byteorder="big", signed=True)
                        / self.frac_off
                    )

                    self.cross_correlation[r0 // 2][r1 // 2][channel][
                        r0 % 2 * 2 + r1 % 2
                    ] = complex(data_re, data_im)

        # Read auto-correlation of R200R200,R201R201
        for i in range(200, 202, 1):
            r0 = i
            r1 = i
            self.polarization = r0 % 2

            data_re = self.xengine_file.read(3)
            data_re = (
                int.from_bytes(data_re, byteorder="big", signed=True) / self.frac_off
            )
            self.auto_correlation[r0 // 2][channel][r0 % 2] = data_re

        # Skip 2 bytes
        self.xengine_file.read(2)

        # 3: Cross-corrleation among different blocks
        for i in range(0, 300, 1):
            for r_head in range(0, 8, 1):
                for r_rear in range(0, 8, 1):
                    r0 = r_head + (self.block_cros[i][0] - 1) * 8
                    r1 = r_rear + (self.block_cros[i][1] - 1) * 8

                    self.polarization = r0 % 2 * 2 + r1 % 2

                    data_re = self.xengine_file.read(3)
                    data_re = (
                        int.from_bytes(data_re, byteorder="big", signed=True)
                        / self.frac_off
                    )

                    data_im = self.xengine_file.read(3)
                    data_im = (
                        int.from_bytes(data_im, byteorder="big", signed=True)
                        / self.frac_off
                    )

                    self.cross_correlation[r0 // 2][r1 // 2][channel][
                        r0 % 2 * 2 + r1 % 2
                    ] = complex(data_re, data_im)

        # 4: R200 R201
        for block_i in range(0, 25, 1):
            for r26 in range(200, 202, 1):
                for half in range(0, 4, 1):
                    r0 = block_i * 8 + half
                    r1 = r26

                    self.polarization = r0 % 2 + r1 % 2 * 2

                    data_re = self.xengine_file.read(3)
                    data_re = (
                        int.from_bytes(data_re, byteorder="big", signed=True)
                        / self.frac_off
                    )

                    data_im = self.xengine_file.read(3)
                    data_im = (
                        int.from_bytes(data_im, byteorder="big", signed=True)
                        / self.frac_off
                    )

                    self.cross_correlation[r0 // 2][r1 // 2][channel][
                        r0 % 2 + r1 % 2 * 2
                    ] = complex(data_re, data_im)

            for r26 in range(200, 202, 1):
                for half in range(4, 8, 1):
                    r0 = block_i * 8 + half
                    r1 = r26

                    self.polarization = r0 % 2 + r1 % 2 * 2

                    data_re = self.xengine_file.read(3)
                    data_re = (
                        int.from_bytes(data_re, byteorder="big", signed=True)
                        / self.frac_off
                    )

                    data_im = self.xengine_file.read(3)
                    data_im = (
                        int.from_bytes(data_im, byteorder="big", signed=True)
                        / self.frac_off
                    )

                    data = abs(complex(data_re, data_im))
                    self.cross_correlation[r0 // 2][r1 // 2][channel][
                        r0 % 2 + r1 % 2 * 2
                    ] = complex(data_re, data_im)
        return True

    @property
    def frequency(self):
        """Calculate Frequency range

        :return frequency of numpy.array
        """
        freq = []
        if self.mode == "LOOP":
            BandAvg = self.frame_head.bandwidth
            for BandInx in range(1, 5):
                BdAvgInc = BandAvg / 2.0 - 0.5

                if BandInx == 1:
                    Frequency = (15 + BdAvgInc) * 1000000 * 125 / 128
                else:
                    Frequency = (
                        (15 + BdAvgInc) * 1000000 * 125 / 128
                        + (BandInx - 2) * 100 * 1000000
                        + 85 * 1000000
                    )
                chan_band = 125 * 1000000 * BandAvg / 128
                frequency = numpy.arange(self.n_channels) * chan_band + Frequency
                freq = freq + frequency.tolist()
        else:
            BandInx = self._band_dict[self.frame_head.freq_band]
            BandAvg = self.frame_head.bandwidth
            BdAvgInc = BandAvg / 2.0 - 0.5

            if BandInx == 1:
                Frequency = (15 + BdAvgInc) * 125 / 128
            else:
                Frequency = (15 + BdAvgInc) * 125 / 128 + (BandInx - 2) * 100 + 85
            chan_band = 125 * BandAvg / 128
            Frequency *= 1000000
            chan_band *= 1000000
            frequency = numpy.arange(self.n_channels) * chan_band + Frequency
            freq = freq + frequency.tolist()
        return freq

    def process_3byte_to_float(self, data_3byte, is_complex=False):
        reshaped = data_3byte.reshape(-1, 3)
        sign_mask = (reshaped[:, 0] >= 0x80).astype(numpy.uint8)
        padded = numpy.zeros((len(reshaped), 4), dtype=numpy.uint8)
        padded[:, 0] = sign_mask * 0xFF
        padded[:, 1:] = reshaped
        values = padded.ravel().view(">i4").astype(numpy.float64) / self.frac_off
        if is_complex:
            return values[::2] + 1j * values[1::2]
        return values

    # Test funcing

    def init_index(self):
        """
        预生成所有索引
        """

        # 块内自相关索引
        ant_indices = numpy.arange(N_BLOCKS * ANTS_PER_BLOCK)  # 0-199
        self.ant_idx = ant_indices // 2  # 0-99
        self.pol_idx = ant_indices % 2  # 0 or 1

        # 块内交叉相关索引
        r_head = numpy.arange(ANTS_PER_BLOCK)
        r_rear = numpy.arange(ANTS_PER_BLOCK)
        head_grid, rear_grid = numpy.meshgrid(r_head, r_rear, indexing="ij")
        valid_mask = (rear_grid > head_grid) & ~(
            (rear_grid % 2 == 1) & (head_grid + 1 == rear_grid)
        )
        r0_base = head_grid[valid_mask]
        r1_base = rear_grid[valid_mask]
        pol_base = r0_base % 2 * 2 + r1_base % 2
        block_offsets = numpy.arange(N_BLOCKS) * ANTS_PER_BLOCK
        self.r0_intra = (
            r0_base[numpy.newaxis, :] + block_offsets[:, numpy.newaxis]
        ).ravel()
        self.r1_intra = (
            r1_base[numpy.newaxis, :] + block_offsets[:, numpy.newaxis]
        ).ravel()
        self.pol_intra = numpy.tile(pol_base, N_BLOCKS)

        # 块间交叉相关索引
        block_pairs = numpy.array(self.block_cros) - 1
        r0_block = numpy.repeat(block_pairs[:, 0] * ANTS_PER_BLOCK, 64)
        r1_block = numpy.repeat(block_pairs[:, 1] * ANTS_PER_BLOCK, 64)
        r_head, r_rear = numpy.meshgrid(
            numpy.arange(ANTS_PER_BLOCK), numpy.arange(ANTS_PER_BLOCK), indexing="ij"
        )
        r0_offset = numpy.tile(r_head.ravel(), len(self.block_cros))
        r1_offset = numpy.tile(r_rear.ravel(), len(self.block_cros))
        self.r0_cross = r0_block + r0_offset
        self.r1_cross = r1_block + r1_offset
        self.pol_cross = self.r0_cross % 2 * 2 + self.r1_cross % 2

        # R200/R201 交叉相关索引
        self.r1_r200 = numpy.repeat([100], N_BLOCKS * ANTS_PER_BLOCK * 2)
        # 生成 base 序列：[0, 0, 2, 2, 4, 4, ..., 98, 98]
        base = numpy.arange(0, 100, 2).repeat(2)
        # 每个单元的偏移量模式：[0, 0, 1, 1]
        pattern = numpy.array([0, 0, 1, 1])
        # 使用广播生成完整序列
        self.r0_r200 = base[:, numpy.newaxis] + pattern
        self.r0_r200 = self.r0_r200.ravel()
        self.pol_r200 = numpy.tile([0, 1, 0, 1, 2, 3, 2, 3], 50)

    def read_one_channel_from_frame(self, channel):
        """
        Read Auto-Correlation and Cross-Correlation data for a specific channel using fully vectorized NumPy operations,
        optimized for maximum performance while ensuring correctness.

        :param channel: Channel number to read (0 to n_channels-1)
        :return: Reading result (True/False)
        """
        block_size = (
            ANTS_PER_BLOCK * AUTO_SIZE_PER_ANT
            + INTRA_CROSS_PAIRS_PER_BLOCK * CROSS_SIZE_PER_PAIR
        )  # 168 bytes
        last_auto_size = 2 * AUTO_SIZE_PER_ANT  # 6 bytes
        padding_size = 2
        cross_block_pairs = (
            len(self.block_cros) * ANTS_PER_BLOCK * ANTS_PER_BLOCK
        )  # 19200 pairs
        cross_block_size = cross_block_pairs * CROSS_SIZE_PER_PAIR  # 115200 bytes
        r200_pairs = N_BLOCKS * ANTS_PER_BLOCK * 2  # 400 pairs
        r200_size = r200_pairs * CROSS_SIZE_PER_PAIR  # 2400 bytes

        total_size = (
            block_size * N_BLOCKS
            + last_auto_size
            + padding_size
            + cross_block_size
            + r200_size
        )  # 121808 bytes

        raw_data = self.xengine_file.read(total_size)
        if len(raw_data) < total_size:
            log.error(
                f"Not enough data to read frame for channel {channel}, expected {total_size}, got {len(raw_data)}"
            )
            return False

        data_array = numpy.frombuffer(raw_data, dtype=numpy.uint8)

        # 数据切片和处理
        offset = 0
        # 1. 块内自相关和交叉相关
        block_data = data_array[offset : offset + block_size * N_BLOCKS]
        auto_data = numpy.concatenate(
            [
                block_data[i : i + ANTS_PER_BLOCK * AUTO_SIZE_PER_ANT]
                for i in range(0, block_size * N_BLOCKS, block_size)
            ]
        )
        auto_values = self.process_3byte_to_float(auto_data)  # 200 values
        self.auto_correlation[self.ant_idx, channel, self.pol_idx] = auto_values

        cross_data = numpy.concatenate(
            [
                block_data[i + ANTS_PER_BLOCK * AUTO_SIZE_PER_ANT : i + block_size]
                for i in range(0, block_size * N_BLOCKS, block_size)
            ]
        )
        cross_values = self.process_3byte_to_float(
            cross_data, is_complex=True
        )  # 600 complex values
        self.cross_correlation[
            self.r0_intra // 2, self.r1_intra // 2, channel, self.pol_intra
        ] = cross_values
        offset += block_size * N_BLOCKS

        # 2. R200/R201 自相关
        last_auto_data = data_array[offset : offset + last_auto_size]
        last_auto_values = self.process_3byte_to_float(last_auto_data)
        self.auto_correlation[100, channel, 0] = last_auto_values[0]
        self.auto_correlation[100, channel, 1] = last_auto_values[1]
        offset += last_auto_size

        # 3. 跳过填充
        offset += padding_size

        # 4. 块间交叉相关
        cross_block_data = data_array[offset : offset + cross_block_size]
        cross_block_values = self.process_3byte_to_float(
            cross_block_data, is_complex=True
        )  # 19200 complex values
        self.cross_correlation[
            self.r0_cross // 2, self.r1_cross // 2, channel, self.pol_cross
        ] = cross_block_values
        offset += cross_block_size
        # 5. R200/R201 交叉相关
        r200_data = data_array[offset : offset + r200_size]
        r200_values = self.process_3byte_to_float(
            r200_data, is_complex=True
        )  # 400 complex values
        self.cross_correlation[self.r0_r200, self.r1_r200, channel, self.pol_r200] = (
            r200_values
        )

        return True
