"""
MUSER Xengine Data Frame Reader.
Version 1.00
Copyright by Astrolab Team in Guangzhou University
"""

import copy
import logging
from datetime import datetime
from os import R_OK, access
from os.path import isfile
from struct import unpack

import numpy
from astropy.time import Time

from museros.components.io.xengine_museri import XEngineMuserIFrame
from museros.components.io.xengine_muserl import XEngineMuserLowFrame

log = logging.getLogger("muser-logger")
log.setLevel(logging.DEBUG)


class MuserXEngine(object):
    def __init__(self, muser_array=None, file_name=None):
        """
        Constructor of XEngine for the MUSER

        :param filename: File name
        :param muser_array Muser Array Name - MUSERL, MUSERI and MUSERH
        """

        if not isinstance(muser_array, str):
            raise "Muser array must be defined as a string (MUSER-H,I,L)"

        self.muser_array = muser_array.lower()

        if self.muser_array not in ["muserl", "museri", "muserh"]:
            raise "MUSER ARRAY: Array name error."
        self.xengine_file = None

        self.frame = None
        if self.muser_array == "muserl":
            self.frame = XEngineMuserLowFrame()
        elif self.muser_array == "museri":
            self.frame = XEngineMuserIFrame()
        # elif muser_array.upper() == "MUSERH":
        #     self.frame = XEngineMuserIFrame()# TO BE DONE
        if file_name is not None:
            self.file_name = file_name
            try:
                if not self.open():
                    log.error("Cannot open the filename")
                    self.file_name = None
                else:
                    self.first_date_time = self.date_time
            except FileNotFoundError:
                self.close()
                self.file_name = None

    def _init_frame_buffer(self):
        # Initial Visibilities of one full frame
        self.vis_data = numpy.zeros(
            (
                self.frame.n_antennas * (self.frame.n_antennas - 1) // 2
                + self.frame.n_antennas,
                self.frame.n_channels * self.frame.n_bands,
                self.frame.n_polarizations,
            ),
            dtype=numpy.complex128,
        )

    def open(self, filename=None):
        """Open MUSER raw data

        :return: open status (True/False)
        """
        if filename is not None:
            self.file_name = filename

        if isfile(self.file_name) and access(self.file_name, R_OK):
            self.frame.xengine_file = open(self.file_name, "rb")
            if self.frame.get_info() == False:
                raise "Cannot read information from the specified files. "
            log.debug("Xengine: Open file successfully.")
            # Init buffer
            self._init_frame_buffer()
            log.debug("Xengine: initialized frame buffer")
            self.__first_frame_utc_time = self.frame.date_time
            return True
        else:
            log.error("Xengine: Cannot open the file.")
        self.xengine_file = None
        return False

    def read_one_frame(self):
        """Read one frame"""
        # Get info from frame header
        if not self.frame.read_frame_head:
            log.error("Xengine: Reading frame header error.")

        obs_time = Time(self.date_time, scale="utc")
        self.__current_frame_utc_time = obs_time
        # Read one frame
        if self.muser_array == "muserl":
            for channel in range(self.n_channels):
                self.frame.read_one_channel_from_frame(channel)
        elif self.muser_array == "museri":
            if not self.frame.read_frame_p0_p6():
                log.error("Xengine: Reading frame (p0-p6) error.")
            if not self.frame.read_frame_p7_p13():
                log.error("Xengine: Reading frame (p7-p13) error.")
            if not self.frame.read_frame_p14_p34():
                log.error("Xengine: Reading frame (p14-p34) error.")
        elif self.muser_array == "muserh":
            if not self.frame.read_frame_p0_p6():
                log.error("Xengine: Reading frame (p0-p6) error.")
            if not self.frame.read_frame_p7_p13():
                log.error("Xengine: Reading frame (p7-p13) error.")
            if not self.frame.read_frame_p14_p34():
                log.error("Xengine: Reading frame (p14-p34) error.")

        log.debug(f"Xengine: Current file pointer {self.frame.xengine_file.tell()}")

        return True

    def read_frame(self):
        """Read full frame

        :return True/False
        """

        for count in range(self.frame.n_full_frames):
            if not self.read_one_frame():
                log.error("File reading error. ")
                exit(1)

            obs_time = Time(self.date_time, scale="utc")
            log.info(f"No.{count} : Observation time (UTC): {obs_time}")

            # Inject data into visibility
            if self.muser_array == "muserl":
                self.vis_data[
                    :, count * self.n_channels : (count + 1) * self.n_channels, :
                ] = self.correlation[:, :, :]
            elif self.muser_array == "museri":
                log.info(
                    f"vis_data shape {self.vis_data.shape} Correlation shape {self.correlation.shape}"
                )
                if self.is_loop_mode:
                    self.vis_data[
                        :,
                        count * self.n_channels : (count + 1) * self.n_channels,
                        self.polarization,
                    ] = self.correlation[:, :]
                else:
                    self.vis_data[:, :, 0] = self.correlation[:, :]
            if count == 0:
                self.__current_frame_utc_time = obs_time
        return True

    def close(self):
        """
        Close file
        """
        if self.frame.xengine_file is not None:
            try:
                self.frame.xengine_file.close()
            finally:
                self.frame.xengine_file = None

    @property
    def get_info(self):
        """Get information of the specified fiel

        :return: Descriptions of the file
        """
        return self.frame.get_info()

    @property
    def n_baselines(self):
        """Return the number of baselines

        :return: n_baselines
        """
        return self.n_antennas * (self.n_antennas - 1) // 2 + self.n_antennas

    @property
    def auto_correlation(self):
        """Return auto correlation data

        :return: auto_correlation
        """
        return copy.deepcopy(self.frame.auto_correlation)

    @property
    def auto_correlation_square(self):
        """Return auto correlation data

        :return: auto_correlation_squqre
        """
        return copy.deepcopy(self.frame.auto_correlation_square)

    @property
    def cross_correlation(self):
        """Return cross correlation data

        :return: cross_correlation
        """
        return copy.deepcopy(self.frame.cross_correlation)

    @property
    def correlation(self):
        """Return baseline-based cross correlation data + auto

        :return: correlation data in one frame
        """
        if self.muser_array == "muserl":

            # fill auto-correlation XY  YX polarizations
            auto_fill_zeros = numpy.zeros(
                (self.n_channels, self.n_polarizations // 2),
                dtype=numpy.complex128,
            )

            data = numpy.zeros(
                (
                    self.frame.n_baselines,
                    self.n_channels,
                    self.n_polarizations,
                ),
                dtype=numpy.complex128,
            )

            index = 0
            for ant1 in range(self.frame.n_antennas):
                for ant2 in range(ant1, self.frame.n_antennas):
                    if ant1 == ant2:
                        data[index, :, 0] = self.frame.auto_correlation[ant1, :, 0]
                        data[index, :, 1] = self.frame.auto_correlation[ant1, :, 0]
                        # Fill zeros?
                        # data[index, :, 1:3] = auto_fill_zeros
                        data[index, :, 2] = self.frame.auto_correlation[ant1, :, 1]
                        data[index, :, 3] = self.frame.auto_correlation[ant1, :, 1]
                    else:
                        data[index, :] = self.frame.cross_correlation[ant1, ant2, :]
                    index += 1

        elif self.muser_array == "museri":
            data = numpy.zeros(
                (self.frame.n_baselines, self.n_channels),
                dtype=numpy.complex128,
            )
            index = 0
            for ant1 in range(self.frame.n_antennas):
                for ant2 in range(ant1, self.frame.n_antennas):
                    if ant1 == ant2:
                        data[index, :] = self.frame.auto_correlation[ant1, :]
                    else:
                        data[index, :] = self.frame.cross_correlation[ant1, ant2, :]
                    index += 1

        return copy.deepcopy(data)

    @property
    def n_frames(self):
        """
        Retrieve number of frames

        :return: number of frames
        """
        return self.frame.n_frames

    @property
    def band(self):
        """
        Retrieve band info

        :return: band start, for muser-low (1,2,3,4)
        """
        return self.frame.band

    @property
    def bandwidth(self):
        """
        Retrieve bandwidth

        :return: bandwidth (1M, 4M, and 16M)
        """
        return self.frame.frame_head.bandwidth

    @property
    def n_channels(self):
        """Return number of channels

        :return: the number of channels
        """
        return self.frame.n_channels

    @property
    def mode(self):
        """Read observation mode (loop/fixed)

        :return: mode value (LOOP/FIXED)
        """
        return self.frame.mode

    @property
    def first_frame_utc_time(self):
        """Read the UTC time in the frame

        :return: UTC time (Astropy Time)
        """
        return Time(self.__first_frame_utc_time, scale="utc")

    @property
    def current_frame_utc_time(self):
        """Read the UTC time of the current frame

        :return: UTC time (Astropy Time)
        """
        return Time(self.__current_frame_utc_time, scale="utc")

    @property
    def date_time(self):
        """
        Retrieve frame date and time

        :return: datetime
        """
        return self.frame.date_time

    @property
    def n_polarizations(self):
        """Return the number of polarizations

        :return: the number of polarizations (2 or 4)
        """
        return self.frame.n_polarizations

    @property
    def polarization(self):
        """Return the polarization of the current frame

        :return: the polarization of the current frame
        """

        return self.frame.polarization

    @property
    def source(self):
        """Read observational source

        :return: source Index
        """
        return self.frame._source_dict[self.frame.frame_head.source]

    @property
    def frequency(self):
        """Get Frequency list of the full frame

        :return: Frequency list of the full frame
        """
        return self.frame.frequency

    @property
    def n_full_frames(self):
        """Get the number of frames in one full frame

        :return: the number of frames
        :rtype: int
        """
        return self.frame.n_full_frames

    @property
    def n_bands(self):
        """Get the number of the bands

        :return: the number of the bands
        :rtype: int
        """
        return self.frame.n_bands

    @property
    def n_antennas(self):
        """Get the number of the antennas

        :return: the number of the antennas
        :rtype: int
        """
        return self.frame.n_antennas

    @property
    def is_loop_mode(self):
        """Flag of observational model

        :return: True(loop)/False(Fixed)
        """
        if self.frame.mode == "LOOP":
            return True
        else:
            return False
