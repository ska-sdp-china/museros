# Data receiver for Xengine

import copy
import logging
from datetime import datetime
from os import R_OK, access
from os.path import isfile
from struct import unpack

import numpy

log = logging.getLogger("muser-logger")


class XEngineMuserIFrameHeader:
    """
    Frame of XEngine Used in MUSER
    """

    def __init__(self):
        """
        Constructor of XengineFrameHeader
        """
        self.year = None
        self.month = None
        self.day = None
        self.hour = None
        self.minute = None
        self.second = None
        self.milli_second = None
        self.micro_second = None
        self.polarization = None
        self.freq_band = None
        self.delay_switch = None
        self.phase_switch = None
        self.source = None
        self.bandwidth = None
        self.number_of_frames = None
        self.integrate_period = None

    def __repr__(self):
        """Represeation

        :return: Frame head information
        """
        return f'Frame:("{self.year}","{self.month}","{self.day}","{self.hour}","{self.minute}","{self.second}", "{self.milli_second}","{self.micro_second}","{self.polarization}","{self.freq_band}")'

    def __str__(self):
        """Class information

        :return: Description
        """
        return f"Frame: {self.year}-{self.month:02d}-{self.day:02d} {self.hour:02d}:{self.minute:02d}:{self.second:02d}.{self.milli_second}{self.micro_second} - Pol:{self.polarization:X}, Band:{self.freq_band:X}, Bandwidth:{self.bandwidth}"


class XEngineMuserIFrame:
    def __init__(self, xengine_file=None):
        """Constructor of XengineFrame

        :param xengine_file: _description_, defaults to None
        """

        # File handler
        self.xengine_file = xengine_file
        # Frame header
        self.frame_head = XEngineMuserIFrameHeader()

        self.number_of_frame_dict = {1: 400, 4: 100, 16: 25}
        self.polarization_dict = {0x33: 0, 0xCC: 1, 0xA3: 0, 0xAC: 1}
        self._polarization_dict = {0: "LL", 1: "RR"}

        # source list
        self._source_dict = {0: "SUN", 1: "SAT", 2: "OTHER"}

        self._channel_dict = {
            0x33: 1,  # 30~80
            0x77: 2,  # 100~200
            0xBB: 3,  # 200~300
            0xCC: 4,  # 300~400
            0xA3: 1,  # Loop Mode
            0xAA: 4,
        }
        self._band_dict = {
            0x33: 1,  # Fix Mode, one band
            0x77: 2,
            0xBB: 3,
            0xCC: 4,
            0xAA: 4,  # Loop Mode, 4 bands
        }

        self._number_of_bands_dict = {
            0x33: 1,  # Fix Mode, one band
            0x77: 1,
            0xBB: 1,
            0xCC: 1,
            0xA3: 4,  # Loop Mode, 4 bands
            0xA7: 4,
            0xAB: 4,
            0xAC: 4,
            0xAA: 4,  # Loop Mode, 4 bands
        }

        self.channel_dict = {0x33: 400, 0x77: 800, 0xBB: 1200, 0xCC: 1600}
        self.freq_band_width = 400

        self.polarization = None
        self.n_polarizations = 1

        self.n_antennas = 40
        self.n_channels = None
        self.n_baselines = None
        self.auto_correlation = None
        self.auto_correlation_square = None
        self.cross_correlation = None
        self._init_data = None
        self.n_bands = None
        self.dr_output_antennas = 44

    def __del__(self):
        if self.xengine_file is not None:
            self.xengine_file.close()

    @property
    def mode(self):
        """Observation mode

        :return: mode (LOOP/FIXED)
        :rtype: str
        """
        if self.frame_head.freq_band in [0x33, 0x77, 0xBB, 0xCC]:
            return "FIXED"
        else:
            return "LOOP"

    @property
    def is_loop_mode(self):

        if self.mode == "LOOP":
            return True
        return False

    @property
    def band(self):
        """Get current band (1-4)

        :return current band
        """
        return self._band_dict[self.frame_head.freq_band]

    @property
    def date_time(self):
        """Observational date and time

        :return: value(datetime)
        """
        ms = self.frame_head.milli_second * 1000 + self.frame_head.micro_second
        value = datetime(
            self.frame_head.year,
            self.frame_head.month,
            self.frame_head.day,
            self.frame_head.hour,
            self.frame_head.minute,
            self.frame_head.second,
            ms,
        )
        return value

    @property
    def n_frames(self):
        """Get the number of frames (period cycle) in the specified file

        :return: n_frames
        """
        return self.frame_head.number_of_frames

    @property
    def channel_bandwidth(self):
        """Get channel width

        :return channel_bandwidth
        """
        return self.frame_head.bandwidth * 125 / 128 * 1e6

    @property
    def n_full_frames(self):
        """The number of frames in a FULL frame

        :return: the number of frames
        """
        if self.is_loop_mode:
            # For Muser I
            return self.n_bands * self.n_polarizations
        return 1

    @property
    def bandwidth(self):
        """Bandwidth in the channel (1/4/8M)

        :return: channelbandwidth
        """
        return self.frame_head.bandwidth

    @property
    def read_frame_head(self):
        """Read XEngine Frame

        :return: Reading result (True/False)
        """
        if self.xengine_file is None:
            return False
        buff = self.xengine_file.read(20)
        (
            self.frame_head.year,
            self.frame_head.month,
            self.frame_head.day,
            self.frame_head.hour,
            self.frame_head.minute,
            self.frame_head.second,
            self.frame_head.milli_second,
            self.frame_head.micro_second,
            self.frame_head.polarization,
            self.frame_head.freq_band,
            self.frame_head.delay_switch,
            self.frame_head.phase_switch,
            self.frame_head.source,
            self.frame_head.bandwidth,
            self.frame_head.number_of_frames,
            self.frame_head.integrate_period,
        ) = unpack("<HBBBBBHHBBBBBBHB", buff)

        log.debug(
            f"Frame Information-{self.frame_head.year}-{self.frame_head.month}-{self.frame_head.day}T{self.frame_head.hour}:{self.frame_head.minute} Band:{self.frame_head.bandwidth} Frames:{self.frame_head.number_of_frames}: Int period: {self.frame_head.integrate_period}"
        )
        self.polarization = self.polarization_dict[self.frame_head.polarization]

        if self._init_data is None:
            # Frame data
            self.n_channels = self.number_of_frame_dict[self.frame_head.bandwidth]

            self.n_baselines = (
                self.n_antennas * (self.n_antennas - 1) // 2 + self.n_antennas
            )
            self.auto_correlation = numpy.ndarray(
                (self.n_antennas, self.n_channels),
                dtype=numpy.float64,
            )
            self.auto_correlation_square = numpy.ndarray(
                (self.n_antennas, self.n_channels),
                dtype=numpy.float64,
            )
            self.cross_correlation = numpy.ndarray(
                (self.n_antennas, self.n_antennas, self.n_channels),
                dtype=numpy.complex128,
            )
            self.n_bands = self._number_of_bands_dict[self.frame_head.freq_band]
            self._init_data = True

        # TODO - multiple polarizations
        log.debug(f"Xengine: File pointer after header {self.xengine_file.tell()}")

        return True

    def read_frame_p0_p6(self):
        """
        Read Auto-Correlation From Frame type 1 (P0-P6)

        :return: Reading result (True/False)
        """
        # p0 - p6
        log.debug(f"Xengine: File pointer before p0-6 {self.xengine_file.tell()}")
        log.debug(f"Xengine: Channels={self.n_channels}")
        for p in range(7):
            for channel in range(self.n_channels):
                # log.info(f"Reading Type-1 frame: {p:2d} - {channel:2d}.")
                # Read RiRi->Ri+5Ri+5 auto_correlation
                buff = self.xengine_file.read(24)
                auto_temp = numpy.array(unpack(">iiiiii", buff), dtype=numpy.int32)
                # 00000000 00000000 00000000 00.000000
                auto_temp = auto_temp // 64 + (auto_temp % 64) / 64.0
                if p == 6:
                    self.auto_correlation[p * 6 : p * 6 + 4, channel] = auto_temp[0:4]
                else:
                    self.auto_correlation[p * 6 : p * 6 + 6, channel] = auto_temp
                # Read RiRi->Ri+5Ri+5 auto_correlation_square
                buff = self.xengine_file.read(48)
                auto2_temp = numpy.array(unpack(">qqqqqq", buff), dtype=numpy.int64)
                auto2_temp = auto2_temp // 4096 + (auto2_temp % 4096) / 4096.0
                if p == 6:
                    self.auto_correlation_square[p * 6 : p * 6 + 4, channel] = (
                        auto2_temp[0:4]
                    )
                else:
                    self.auto_correlation_square[p * 6 : p * 6 + 6, channel] = (
                        auto2_temp
                    )
        log.debug(f"Xengine: File pointer after p0-6 {self.xengine_file.tell()}")

        return True

    def read_frame_p7_p13(self):
        """
        Read CrossCorrelation Frame type 2 (P7-P13)

        :return: Reading result (True/False)
        """
        # p7 - p13
        for p in range(7):
            for channel in range(self.n_channels):
                # log.info(f"Reading Type-2 frame: {p:2d} - {channel:2d}.")
                # Read RiRi->Ri+5Ri+5 auto_correlation
                buff = self.xengine_file.read(120)
                format = ">" + "i" * 30
                result = unpack(format, buff)
                correlation_tmp = numpy.array(result, dtype=numpy.int32)
                correlation_tmp = correlation_tmp // 64 + (correlation_tmp % 64) / 64.0
                correlation = correlation_tmp[0::2] + 1j * correlation_tmp[1::2]
                index = 0
                for antenna1 in range(p * 6, p * 6 + 5):
                    for antenna2 in range(antenna1 + 1, p * 6 + 6):
                        if antenna1 < self.n_antennas and antenna2 < self.n_antennas:
                            self.cross_correlation[antenna1, antenna2, channel] = (
                                correlation[index]
                            )
                        index += 1
        log.debug(f"Xengine: File pointer after p7-13 {self.xengine_file.tell()}")
        return True

    def read_frame_p14_p34(self):
        """
        Read CrossCorrelation Frame type 3 (P14-P34)

        :return: Reading result (True/False)
        """
        # p14 - p34
        frame_order = (
            (0, 1),
            (0, 2),
            (0, 3),
            (0, 4),
            (0, 5),
            (0, 6),
            (5, 6),
            (1, 2),
            (1, 3),
            (1, 4),
            (1, 5),
            (1, 6),
            (4, 5),
            (4, 6),
            (2, 3),
            (2, 4),
            (2, 5),
            (1, 6),
            (3, 4),
            (3, 5),
            (3, 6),
        )

        for bstart, bend in frame_order:
            for channel in range(self.n_channels):
                # log.info(f"Reading Type-3 frame: B{bstart}-B{bend}.")

                buff = self.xengine_file.read(288)
                format = ">" + "i" * 72
                result = unpack(format, buff)
                correlation_tmp = numpy.array(result, dtype=numpy.int32)
                correlation_tmp = correlation_tmp // 64 + correlation_tmp % 64 * 1e-6

                correlation = correlation_tmp[0::2] + 1j * correlation_tmp[1::2]
                index = 0
                for antenna1 in range(6):
                    for antenna2 in range(6):
                        ant1 = bstart * 6 + antenna1
                        ant2 = bend * 6 + antenna2
                        if ant1 < self.n_antennas and ant2 < self.n_antennas:
                            self.cross_correlation[ant1, ant2, channel] = correlation[
                                index
                            ]
                        index += 1

        return True

    def get_info(self):
        """
        Get information of the file
        """
        log.info(f"Xengine: Get current file information")
        if not self.read_frame_head:
            log.error("Xengine: Get frame header error.")
            return False
        # Move the file pointer to the beginning
        self.xengine_file.seek(0)
        log.debug(
            f"Xengine: frame info: {self.date_time}:{self.bandwidth}:{self.n_frames}:{self.n_channels}"
        )
        self.frame_number = self.n_channels * self.n_bands * self.n_polarizations

        return True
