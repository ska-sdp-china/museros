"""
MUSER Xengine Data File Information.
Version 1.00
Copyright by Astrolab Team in Guangzhou University
"""

import argparse
import logging
import os
import sys
from datetime import datetime
from os.path import isfile

from museros.apps.apps_parser import apps_parser_app, apps_parser_export_files
from museros.components.io.xengine_muser import MuserXEngine
from museros.data_models.muser_data import MuserData
from museros.data_models.parameters import (
    muser_data_list,
    muser_data_path,
    muser_output_path,
    muser_path,
)

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def get_file_info_from_path(args):
    if args.data_path is None:
        raise "You need to specify a diretory"
    muser_array = args.muser_array.upper()
    data_path = args.data_path
    file_list = muser_data_list(muser_array, data_path)
    file_info = []
    print("file_list----", file_list)

    if len(file_list) == 0:
        print("Cannot find any file.")
        return False

    for file_name in file_list:
        full_file_name = data_path + file_name
        print(full_file_name)

        xfile = MuserXEngine(file_name=full_file_name, muser_array=muser_array)

        if not xfile.open(full_file_name):
            print("Error: No files exist.")
            exit(-1)

        # xfile.get_info()
        xfile.read_frame()  # For MUSERL, polarization info not in frame head

        if xfile.mode == "LOOP":
            band = "000-400"
            polarization = "AA"
        elif xfile.frame.frame_head.freq_band == 0x33:
            band = "30-100"
            polarization = xfile.frame._polarization_dict[xfile.frame.polarization]
        elif xfile.frame.frame_head.freq_band == 0x77:
            band = "100-200"
            polarization = xfile.frame._polarization_dict[xfile.frame.polarization]
        elif xfile.frame.frame_head.freq_band == 0xBB:
            band = "200-300"
            polarization = xfile.frame._polarization_dict[xfile.frame.polarization]
        elif xfile.frame.frame_head.freq_band == 0xCC:
            band = "300-400"
            polarization = xfile.frame._polarization_dict[xfile.frame.polarization]

        log.info("Checking MUSER File Information ...")
        log.info("Filename: {}".format(full_file_name))
        log.info(
            "First Observational Time: {}".format(xfile.current_frame_utc_time)
        )  # date_time))
        log.info("Source: {}".format(xfile.source))
        log.info(
            "Observational Mode: {} \nBAND: {} \nBandwidth: {} ".format(
                xfile.mode,
                band,
                xfile.frame.frame_head.bandwidth,
            )
        )
        log.info("POL: {}".format(polarization))
        log.info(
            "PHASE SWITCH: {} \nDELAY SWITCH: {}".format(
                xfile.frame.frame_head.phase_switch, xfile.frame.frame_head.delay_switch
            )
        )
        log.info("VALID: {} ".format(xfile.frame.frame_head.valid))
        log.info("NFRAMES: {} ".format(xfile.frame.frame_head.number_of_frames))

        file_info.append(
            {
                "date_time": xfile.date_time,
                "source": xfile.source,
                "mode": xfile.mode,
                "freq_band": band,
                "bandwidth": xfile.frame.frame_head.bandwidth,
                "polarization": polarization,
                "phase_switch": xfile.frame.frame_head.phase_switch,
                "delay_switch": xfile.frame.frame_head.delay_switch,
                "nframes": xfile.frame.frame_head.number_of_frames,
                "valid": xfile.frame.frame_head.valid,
                "nframes": xfile.frame.frame_head.number_of_frames,
                "filename": file_name,
                "filepath": data_path,
            }
        )

    print(file_info)
    return file_info


def get_file_info(args):
    """
    muser_array: MUSER-L, MUSER-I, MUSER-H
    file_name: file path and name
    """
    if args.muser_array is None:
        raise "You have to specify the muser array name"

    if args.file_name is None:
        raise "You have to specify a absoluate directory"

    muser_array = args.muser_array.upper()
    file_name = args.file_name

    xfile = MuserXEngine(muser_array=muser_array, file_name=file_name)

    if not xfile.open(file_name):
        print("Error: No files exist.")
        exit(-1)

    # xfile.get_info()
    xfile.read_frame()  # For MUSERL, polarization info not in frame head

    print(hex(xfile.frame.frame_head.freq_band))

    if xfile.mode == "LOOP":
        band = "000-400"
        polarization = "AA"
    elif xfile.frame.frame_head.freq_band == 0x33:
        band = "30-100"
        polarization = xfile.frame._polarization_dict[xfile.frame.polarization]
    elif xfile.frame.frame_head.freq_band == 0x77:
        band = "100-200"
        polarization = xfile.frame._polarization_dict[xfile.frame.polarization]
    elif xfile.frame.frame_head.freq_band == 0xBB:
        band = "200-300"
        polarization = xfile.frame._polarization_dict[xfile.frame.polarization]
    elif xfile.frame.frame_head.freq_band == 0xCC:
        band = "300-400"
        polarization = xfile.frame._polarization_dict[xfile.frame.polarization]

    log.info("Checking MUSER File Information ...")
    log.info("Filename: {}".format(os.path.basename(file_name)))
    log.info("First Observational Time: {}".format(xfile.date_time))
    log.info("Source: {}".format(xfile.source))
    log.info(
        "Observational Mode: {} \nBAND: {} \nBandwidth: {} ".format(
            xfile.mode,
            band,
            xfile.frame.frame_head.bandwidth,
        )
    )
    log.info("POL: {}".format(polarization))
    log.info(
        "PHASE SWITCH: {} \nDELAY SWITCH: {}".format(
            xfile.frame.frame_head.phase_switch, xfile.frame.frame_head.delay_switch
        )
    )
    log.info("VALID: {} ".format(xfile.frame.frame_head.valid))
    log.info("NFRAMES: {} ".format(xfile.frame.frame_head.number_of_frames))

    file_info = {
        "date_time": xfile.date_time,
        "source": xfile.source,
        "mode": xfile.mode,
        "freq_band": band,
        "bandwidth": xfile.frame.frame_head.bandwidth,
        "polarization": polarization,
        "phase_switch": xfile.frame.frame_head.phase_switch,
        "delay_switch": xfile.frame.frame_head.delay_switch,
        "nframes": xfile.frame.frame_head.number_of_frames,
        "valid": xfile.frame.frame_head.valid,
        "nframes": xfile.frame.frame_head.number_of_frames,
        "filename": os.path.basename(file_name),
        "filepath": os.path.dirname(file_name),
    }

    print(file_info)
    return file_info


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Read MUSER file information. ")
    parser.add_argument(
        "-d", "--data_path", type=str, default=None, help="The Data path"
    )
    parser.add_argument("-f", "--file_name", type=str, default=None, help="A file name")
    parser.add_argument(
        "-m", "--muser_array", type=str, default="MUSERL", help="The MUSER Array"
    )

    args = parser.parse_args()
    if args.file_name is not None:
        get_file_info(args)
    else:
        get_file_info_from_path(args)
