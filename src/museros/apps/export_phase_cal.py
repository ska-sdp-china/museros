"""
Export Phase calibration

"""

import os
import sys

path = os.path.abspath(os.path.dirname(__file__))

import argparse
import datetime
import logging
import traceback

import numpy
from astropy.time import Time

from museros.apps.apps_parser import apps_parser_app, apps_poarser_export_phase_files
from museros.data_models.muser_data import MuserData
from museros.data_models.parameters import (
    muser_calibration_path,
    muser_data_path,
    muser_output_path,
    muser_path,
)

log = logging.getLogger("muser-logger")


def cli_parser():
    """Get a command line parser and populate it with arguments

    First a CLI argument parser is created. Each function call adds more arguments to the parser.

    :return: CLI parser argparse
    """

    parser = argparse.ArgumentParser(
        description="MUSER Phase Calibration file exporter", fromfile_prefix_chars="@"
    )
    parser = apps_parser_app(parser)
    parser = apps_poarser_export_phase_files(parser)

    return parser


class ExportPhase:
    def __init__(
        self,
        muser_array=None,
        input_file=None,
        nframes=1,
        output_file=None,
    ):
        """Constructor for ExportPhase

        :param muser_array: Muser Array, defaults to None, The name should be one in [muserl, museri, muserh]
        :type muser_array: str
        :param input_file: _description_, defaults to None
        :type input_file: str, optional
        :param nframes: the number of the frames for exporting, defaults to 1
        :type nframes: int, optional
        :param output_file: Output file, defaults to None
        :type output_file: str, optional
        """

        self.muser_array = muser_array.lower()
        self.n_frames = nframes

        if input_file is not None:
            self.input_file = input_file
        else:
            self.input_file = None

        if output_file is not None:
            self.output_file = output_file
        else:
            self.output_file = None

    def export_phase_file(self):
        """
        Generate calibration

        :return True for successfully exporting, False for not
        """

        muser_phase = MuserData(muser_array=self.muser_array, file_name=self.input_file)

        log.info("Reading Visibility Data of calibration......")

        # Check data
        log.info("Filename {} is a valid MUSER Data File.".format(self.input_file))
        log.info("First Observational Time {}".format(muser_phase.first_date_time))
        log.info("Loop mode: {}".format(muser_phase.mode))
        log.info("Target: {}".format(muser_phase.source))
        log.info("First frame Sub band {}".format(muser_phase.band))

        total_frames = self.n_frames

        log.info(
            f"SUB ARRAY: {self.muser_array}, MODE: {muser_phase.is_loop_mode}, Total_frames: {total_frames}"
        )

        if muser_phase.is_loop_mode:
            self.block_full_data = numpy.zeros(
                (
                    muser_phase.n_baselines,
                    muser_phase.n_channels * muser_phase.n_bands,
                    muser_phase.n_polarizations,
                ),
                dtype=numpy.complex128,
            )

            count = 0
            while count < total_frames:
                if not muser_phase.read_frame():
                    print("File reading error. ")
                    exit(1)

                log.info(
                    "Reading No. %d %s %d %d"
                    % (
                        count,
                        muser_phase.date_time,
                        muser_phase.band,
                        muser_phase.polarization,
                    )
                )

                self.block_full_data[:, :, :] += muser_phase.vis_data[:, :, :]
                count = count + 1

        else:
            self.block_full_data = numpy.zeros(
                [
                    muser_phase.n_baselines,
                    muser_phase.n_channels,
                    1,
                ],
                dtype=numpy.complex128,
            )
            count = 0
            while count < total_frames:
                if not muser_phase.read_frame():
                    log.error("File reading error. ")
                    raise "File reading error..."

                log.info(
                    "Reading No. %d %s %d %d"
                    % (
                        count,
                        muser_phase.date_time,
                        muser_phase.bandwidth,
                        muser_phase.polarization,
                    )
                )

                self.block_full_data[:, :, 0] += muser_phase.correlation[:, :, 0]
                count += 1

        # Mean Value
        self.block_full_data /= self.n_frames
        # Calculate phase
        self.phase = numpy.arctan2(self.block_full_data.imag, self.block_full_data.real)
        self.year = muser_phase.date_time.year
        self.month = muser_phase.date_time.month
        self.day = muser_phase.date_time.day
        self.hour = muser_phase.date_time.hour
        self.minute = muser_phase.date_time.minute
        self.second = muser_phase.date_time.second
        log.info(f"Phase calibration data shape: {self.phase.shape}")
        log.info(f"The type of phase calibration data: {self.phase.dtype}")
        if self.output_file is None:
            file_name = muser_calibration_path(
                "%s-%04d%02d%02dT%02d%02d.CAL"
                % (
                    self.muser_array.upper(),
                    self.year,
                    self.month,
                    self.day,
                    self.hour,
                    self.minute,
                )
            )
        else:
            file_name = muser_calibration_path(self.output_file)
        log.info("Shape of the Visibilities: ", self.block_full_data.shape)
        print("Writing to file: " + os.path.basename(file_name))
        # Save phase data
        log.info(f"Phase shape: {self.phase.shape}")
        self.phase.tofile(file_name)

        log.info("Export phase done.")
        return True


def export_phase(args):
    """Export phase file

    :param args: arguments
    :return
    """
    if args.logfile is not None:
        logging.basicConfig(
            format="%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s",
            level=logging.DEBUG,
        )

        fh = logging.FileHandler(args.logfile, mode="w")
        fh.setLevel(logging.DEBUG)
        log.addHandler(fh)
    else:
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        log.addHandler(console)
        log.setLevel(level=logging.INFO)
        log.info("Logging device: CONSOLE")

    if args.loglevel is not None:
        if args.loglevel == "INFO":
            log.setLevel(level=logging.INFO)
        elif args.loglevel == "DEBUG":
            log.setLevel(level=logging.DEBUG)
        elif args.loglevel == "WARNING":
            log.setLevel(level=logging.WARNING)
        elif args.loglevel == "ERROR":
            log.setLevel(level=logging.ERROR)
        else:
            log.setLevel(level=logging.INFO)

    log.info("Starting exporting meansurement set file")

    muser = args.muser
    input_file = args.file
    if len(args.output) != 0:
        output_file = args.output
    else:
        output_file = None
    nframes = args.nframes
    cal = ExportPhase(
        muser_array=muser,
        input_file=input_file,
        output_file=output_file,
        nframes=nframes,
    )
    cal.export_phase_file()


def main():
    log.info("Starting exporting phase calibration file")

    parser = cli_parser()
    args = parser.parse_args()
    export_phase(args)


if __name__ == "__main__":
    main()
