import argparse


def apps_parser_app(parser):
    """Add apps-specific command line arguments to an existing CLI parser

    :param parser: argparse
    :return: argparse
    """
    parser.add_argument(
        "-m", "--muser", type=str, default="muserl", help="The MUSER array"
    )

    parser.add_argument(
        "-e",
        "--elevation",
        type=int,
        default=5,
        help="No limitation for the elevation",
    )

    parser.add_argument(
        "--logfile",
        type=str,
        default=None,
        help="Name of logfile (default is to construct one from msname)",
    )

    parser.add_argument(
        "--loglevel",
        type=str,
        default="INFO",
        help="Log level (DEBUG, INFO, WARNING, ERROR)",
    )

    parser.add_argument(
        "--performance_file",
        type=str,
        default=None,
        help="Name of json file to contain performance information",
    )

    return parser


def apps_parser_export_files(parser):
    """Add command line arguments to an existing CLI parser

    :param parser: argparse
    :return: argparse
    """
    parser.add_argument(
        "-c", "--cal", type=str, default="", help="The Calibration file name"
    )
    parser.add_argument("-f", "--file", type=str, default=None, help="The file name")

    parser.add_argument(
        "--old_data",
        type=str,
        default="False",
        help="Old data format",
    )

    parser.add_argument(
        "-fb",
        "--flag_baselines",
        type=str,
        default="",
        help="The file name of flagged baseline",
    )

    parser.add_argument(
        "-fa",
        "--flag_antennas",
        type=str,
        default="",
        help="The file name of flagged antenna",
    )

    parser.add_argument(
        "-o", "--output", type=str, default="", help="The output file name"
    )

    parser.add_argument(
        "-t", "--type", type=str, default="ms", help="File type [ms|fits]"
    )

    parser.add_argument("-n", "--nframes", type=int, default=0, help="Number of frames")

    parser.add_argument("-b", "--bands", type=int, default=1, help="number of bands")
    # parser.add_argument("-f", "--fringe", type=bool, default=False, help="Fringe Stop")

    return parser


def apps_parser_batch_export_files(parser):
    """Add command line arguments to an existing CLI parser

    :param parser: argparse
    :return: argparse
    """
    parser.add_argument(
        "-c", "--cal", type=str, default="", help="The Calibration file name"
    )

    parser.add_argument(
        "-s", "--sourcedir", type=str, default=None, help="The source directory"
    )

    parser.add_argument(
        "-d", "--destdir", type=str, default=None, help="The destination directory"
    )

    parser.add_argument("--nframes", type=int, default=1, help="The number of frames")

    parser.add_argument(
        "--old_data",
        type=str,
        default="False",
        help="Old data format",
    )

    parser.add_argument(
        "-fb",
        "--flag_baselines",
        type=str,
        default="",
        help="The file name of flagged baseline",
    )

    parser.add_argument(
        "-fa",
        "--flag_antennas",
        type=str,
        default="",
        help="The file name of flagged antenna",
    )

    parser.add_argument(
        "-t", "--type", type=str, default="ms", help="File type [ms|fits]"
    )

    parser.add_argument("-n", "--nframe", type=int, default=10, help="Number of frames")

    parser.add_argument("-b", "--bands", type=int, default=1, help="number of bands")
    # parser.add_argument("-f", "--fringe", type=bool, default=False, help="Fringe Stop")

    parser.add_argument(
        "--use_dask", type=str, default="True", help="Use Dask for parallel processing"
    )

    parser.add_argument(
        "--dask_scheduler",
        type=str,
        default=None,
        help=(
            "Externally defined Dask scheduler e.g. 127.0.0.1:8786 or ssh for "
            "SSHCluster or existing for current scheduler"
        ),
    )

    parser.add_argument(
        "--dask_memory",
        type=str,
        default=None,
        help="Memory per Dask worker (GB), e.g. 5GB (None means \
            Dask will choose)",
    )

    parser.add_argument(
        "--dask_nworkers",
        type=int,
        default=None,
        help="Number of workers (None means Dask will choose)",
    )

    parser.add_argument(
        "--dask_nthreads",
        type=int,
        default=None,
        help="Number of threads in each Dask worker (None means Dask \
               will choose)",
    )

    return parser


def apps_poarser_movie_maker_files(parser):
    """Parser for converting movie files
    :param parser: Parser instance

    :return parser: Parser added related arguments
    """
    parser.add_argument(
        "-s", "--sourcedir", type=str, default="", help="The source directory"
    )
    parser.add_argument("-f", "--frequency", type=str, default="", help="Frequency")
    parser.add_argument(
        "-o", "--output", type=str, default="", help="The output filename"
    )

    return parser


def apps_poarser_export_phase_files(parser):
    """Parser for exporting_phase_filtes
    :param parser: Parser instance

    :return parser: Parser added related arguments
    """
    parser.add_argument("-f", "--file", type=str, default="", help="The input file")
    parser.add_argument("-n", "--nframes", type=int, default=1, help="number of frames")
    # parser.add_argument("-n", "--nbands", type=int, default=1, help="number of bands")
    # parser.add_argument("-f", "--fringe", type=bool, default=False, help="Fringe Stop")
    parser.add_argument(
        "-o", "--output", type=str, default="", help="The output filename"
    )

    return parser
