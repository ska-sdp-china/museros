"""
MUSER raw observational file sync.
Version 1.00
Copyright by Astrolab Team in Guangzhou University
"""

import argparse
import logging
import math
import os
import shutil
import stat
import sys
import time
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor, as_completed

import paramiko

__version__ = 0.1
__author__ = "Yangfan Xie"

log = logging.getLogger("muser-logger")
log.setLevel(level=logging.DEBUG)
log.addHandler(logging.StreamHandler(sys.stdout))


def sync_parser():
    parser = argparse.ArgumentParser(
        description="MUSER file sync", fromfile_prefix_chars="@"
    )

    parser.add_argument(
        "-n",
        "--nworker",
        type=int,
        default=10,
        help="how many worker of ProcessPool/ThreadPool",
    )

    parser.add_argument(
        "-e",
        "--exec",
        type=str,
        default="process",
        help="ProcessPool or ThreadPool",
    )

    parser.add_argument("-s", "--srcdir", type=str, help="src dir")

    parser.add_argument("-d", "--dstdir", type=str, help="dst dir")

    parser.add_argument("-u", "--user", type=str, default="root", help="sftp username")

    parser.add_argument(
        "-k", "--key", type=str, default="/root/.ssh/id_rsa", help="sftp key"
    )

    parser.add_argument("-i", "--ip", type=str, help="sftp ip")

    parser.add_argument("-p", "--port", type=int, default=22, help="sftp port")
    parser.add_argument(
        "-c", "--clean", type=str, default="False", help="if clean srcdir"
    )

    return parser


def _get_all_files_in_remote_dir(sftp, remote_dir):
    all_files = list()
    if remote_dir[-1] == "/":
        remote_dir = remote_dir[0:-1]

    files = sftp.listdir_attr(remote_dir)
    for file in files:
        filename = remote_dir + "/" + file.filename

        if stat.S_ISDIR(file.st_mode):
            all_files.extend(_get_all_files_in_remote_dir(sftp, filename))
        else:
            all_files.append(filename)

    return all_files


def make_chunk_path_list(
    src_ip,
    src_port,
    srcpath_dir,
    dstpath_dir,
    pkey_path,
    src_username,
    chunk_number=8,
):
    pkey = paramiko.RSAKey.from_private_key_file(pkey_path)
    trans = paramiko.Transport((src_ip, src_port))
    trans.connect(username=src_username, pkey=pkey)
    sftp = paramiko.SFTPClient.from_transport(trans)

    all_file_list = _get_all_files_in_remote_dir(sftp, srcpath_dir)

    length = len(all_file_list)

    number_per_chunk = int(math.ceil(1.0 * length / chunk_number))

    for i in range(0, length, number_per_chunk):
        chunk_srcpath_list = all_file_list[i : i + number_per_chunk]
        chunk_dispath_list = [
            chunk_src.replace(srcpath_dir, dstpath_dir)
            for chunk_src in chunk_srcpath_list
        ]

        yield chunk_srcpath_list, chunk_dispath_list


def chean_pathlist(path_list):
    for path in path_list:
        if os.path.exists(path):
            os.remove(path)


def rm(sftp, path, rmtop=False):
    files = sftp.listdir_attr(path)
    for file in files:
        filename = path + "/" + file.filename
        if stat.S_ISDIR(file.st_mode):
            rm(sftp, filename, rmtop=True)
        else:
            sftp.remove(filename)
    if rmtop:
        sftp.rmdir(path)


def clean_remote(src_ip, src_port, srcdir, pkey_path, src_username="root"):
    pkey = paramiko.RSAKey.from_private_key_file(pkey_path)
    trans = paramiko.Transport((src_ip, src_port))
    trans.connect(username=src_username, pkey=pkey)
    sftp = paramiko.SFTPClient.from_transport(trans)

    rm(sftp, srcdir)


def chunk_file_job(
    src_ip,
    src_port,
    srcpath_list,
    dstpath_list,
    pkey_path,
    src_username="root",
):
    """Chunk files

    :param src_ip: remote ip
    :param src_port: remote port
    :param srcpath_list: remote file path list
    :param dstpath_list: local file path list
    :param pkey_path: private key for ssh
    :param src_username: remote username, defaults to "root"
    """
    pkey = paramiko.RSAKey.from_private_key_file(pkey_path)
    trans = paramiko.Transport((src_ip, src_port))
    trans.connect(username=src_username, pkey=pkey)

    assert len(srcpath_list) == len(dstpath_list)
    sftp = paramiko.SFTPClient.from_transport(trans)

    for srcpath, dstpath in zip(srcpath_list, dstpath_list):
        # mkdir -p
        dst_dir = os.path.dirname(dstpath)
        if not os.path.exists(dst_dir):
            os.system(f"mkdir -p {dst_dir}")
        # transport
        sftp.get(srcpath, dstpath)
    trans.close()


def try_chunk_file_job(
    src_ip,
    src_port,
    srcpath_list,
    dstpath_list,
    pkey_path,
    src_username,
    retry=1,
):
    is_ok = False
    for i in range(retry):
        try:
            chunk_file_job(
                src_ip,
                src_port,
                srcpath_list,
                dstpath_list,
                pkey_path,
                src_username,
            )
        except:
            log.warning(f"Some wrong !!!,and retry: {i}")
            # clean
            chean_pathlist(dstpath_list)
            time.sleep(2)
        else:
            is_ok = True
            break

    return is_ok


def sync(args):
    log.info("checking... file")
    ip = args.ip
    port = args.port
    key = args.key
    user = args.user
    nworker = args.nworker

    chunk_gen = make_chunk_path_list(
        ip, port, args.srcdir, args.dstdir, key, user, chunk_number=nworker
    )
    log.info("check file done...submit task")
    if args.exec == "process":
        exec_pool = ProcessPoolExecutor(max_workers=nworker)
    elif args.exec == "thread":
        exec_pool = ThreadPoolExecutor(max_workers=nworker)
    else:
        raise ValueError("uknown Executor")

    task_list = []
    for idx, (chunk_srcpath_list, chunk_dispath_list) in enumerate(chunk_gen):
        log.info(f"submit chunk-{idx}: {len(chunk_srcpath_list)}")

        future = exec_pool.submit(
            try_chunk_file_job,
            ip,
            port,
            chunk_srcpath_list,
            chunk_dispath_list,
            key,
            user,
        )

        task_list.append(future)

    for idx, future in enumerate(as_completed(task_list)):
        data = future.result()
        log.info(f"chunk-{idx} is ok? : {data}")

    exec_pool.shutdown()

    # clean
    if args.clean == "True":
        log.info("clean remote dir...")
        clean_remote(ip, port, args.srcdir, key, user)
        log.info("clean done.")

    log.info("sync job done.")


if __name__ == "__main__":
    parser = sync_parser()
    args = parser.parse_args()
    print(args)
    sync(args)
