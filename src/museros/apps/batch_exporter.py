"""
MUSER Xengine Data File Exporter.
"""

import argparse
import datetime
import logging
import os
import re
from collections import OrderedDict

import astropy.units as u
from astropy.time import Time

from museros.apps.apps_parser import apps_parser_app, apps_parser_batch_export_files
from museros.apps.muser_exporter import cli_parser, muser_exporter
from museros.components.io.xengine_muser import MuserXEngine
from museros.components.utils.dask_client import DaskClient, get_dask_client
from museros.components.utils.set_astropy_iers import configure_iers_settings
from museros.data_models.muser_data import MuserData

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)
# log.addHandler(logging.StreamHandler(sys.stdout))


def match_filename(filename):
    """
    Match the file format
    Syntax:
    - Source: String
    - YearMonthDay: 8 Digits(4:Year + 2:Month + 2:day)
    - HHMMSS: Hour, Minute and Second
    - Bandwidth: 1、4 或 8
    - File No.: Integer
    """
    pattern = (
        r"^MUSERL_([a-zA-Z]+)_(\d{4}\d{2}\d{2})_(\d{2}\d{2}\d{2})_W(1|4|8)_(\d+)?$"
    )
    match = re.match(pattern, filename)
    if match:
        # return {No:filename}
        return {eval(match.group(5)): filename}
    return None


def collect_data_list(muser_array, path=None):
    """Retrieve file lists

    :param path:
    :return file list
    """
    if path is None:
        return []
    matched_files_dict = {}
    matched_files = []
    if not os.path.exists(path):
        print(f"Directory {path} does not exist.")
        return matched_files

    for filename in os.listdir(path):
        result = match_filename(filename)
        if result:
            matched_files_dict.update(result)
    if matched_files_dict:
        matched_files_dict = OrderedDict(sorted(matched_files_dict.items()))
        matched_files = list(matched_files_dict.values())
    return matched_files


def get_file_info_read_frame(data_path, file_list):
    """
    Get a list of files with interval of every 5 minutes by reading the first frame of each file.
    Input: file list of a specified directory
    Output: Selected file list

    """

    file_list_with_time = []

    for file_name in file_list:
        full_file_name = data_path + file_name

        xfile = MuserXEngine(file_name=full_file_name, muser_array=muser_array)

        if not xfile.open(full_file_name):
            raise ("Error: No files exist.")

        # xfile.get_info()
        xfile.read_one_frame()  # For MUSERL, polarization info not in frame head
        current_frame_time = xfile.current_frame_utc_time + 8 * u.hour

        log.info("Filename: {}".format(full_file_name))
        log.info("First Observational Time: {}".format(current_frame_time))

        file_list_with_time.append((file_name, current_frame_time))

    # file_list_with_time.sort(key=lambda x: x[1])
    selected_files = []
    last_selected_time = None

    for file_name, file_datetime in file_list_with_time:
        if (
            last_selected_time is None
            or (5.0 - (file_datetime - last_selected_time).to(u.min).value) < 1e-6
        ):
            selected_files.append((file_name))
            last_selected_time = file_datetime

    return selected_files


def get_file_info_from_filename(file_list):
    """
    Get a list of files with interval of every 5 minutes by reading the file name.
    Input: file list of a specified directory
    Output: Selected file list

    """

    selected_files = []
    last_selected_time = None

    for file_name in file_list:

        time_list = file_name.split("_")
        if len(time_list) < 6:
            continue

        date_str = time_list[2]
        time_str = time_list[3]
        # Get the datetime object from the string
        file_time_str = f"{date_str[:4]}-{date_str[4:6]}-{date_str[6:]} {time_str[:2]}:{time_str[2:4]}:{time_str[4:]}"
        file_datetime = Time(file_time_str, scale="utc")

        if (
            last_selected_time is None
            or 5.0 - (file_datetime - last_selected_time).to(u.min).value < 1e-6
        ):
            selected_files.append((file_name))
            last_selected_time = file_datetime

    return selected_files


def export_ms_files(dask_client, file_list, args):
    """Export MS file

    :param dask_client: _description_
    :param data_path: _description_
    :param out_path: _description_
    :param file_list: _description_
    :param nframes: _description_

    :return: _description_
    """
    tasks = []
    nframes = args.nframes
    print(args)
    use_dask = args.use_dask.lower() == "true"
    for file_name in file_list:

        full_file_name = os.path.join(args.sourcedir, file_name)
        log.info("Exporting file: {}".format(full_file_name))
        output_path = os.path.join(args.destdir, file_name + ".ms")

        export_ms_args = [
            "--muser",
            "MUSERL",
            "--file",
            full_file_name,
            "--output",
            output_path,
            "--cal",
            "",
            "--type",
            "ms",
            "--flag_baselines",
            "",
        ]
        if args.nframes:
            export_ms_args.append("--nframes")
            export_ms_args.append(str(args.nframes))
        if args.old_data.lower() == "true":
            export_ms_args.append("--old_data")
            export_ms_args.append(args.old_data)
        parser = cli_parser()
        arg_export_ms = parser.parse_args(export_ms_args)
        tasks.append(dask_client.delayed(muser_exporter)(arg_export_ms))
    return dask_client.compute(tasks, sync=True)


def main():
    """
    Main function for batch_exporter
    """
    parser = argparse.ArgumentParser(description="Read MUSER file information. ")
    parser = apps_parser_app(parser)
    parser = apps_parser_batch_export_files(parser)

    args = parser.parse_args()

    if args.sourcedir is None:
        raise ("You MUST specify a diretory containing observational data.")

    if args.destdir is None:
        raise ("You MUST specify a diretory name for output results.")
    # Create the output directory if it does not exist
    if not os.path.exists(args.destdir):
        os.makedirs(args.destdir)

    if args.logfile is None:
        logfile = os.path.join(args.destdir, "batch_exporter.log")
    else:
        logfile = args.logfile

    def init_logging():
        logging.basicConfig(
            filename=logfile,
            filemode="a",
            format="%(asctime)s.%(msecs)d %(name)s %(levelname)s %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S %p",
            level=logging.DEBUG,
        )

    starttime = datetime.datetime.now()
    log.info("MUSER Batch file exporter Version 1.00")
    print("MUSER Batch file exporter Version 1.00")
    msg = f"Started : {starttime}"
    log.info(msg)
    print("Started : ", starttime)
    # Dask
    use_dask = args.use_dask.lower() == "true"
    dask_client = DaskClient(use_dask=use_dask)

    if use_dask:
        if args.dask_scheduler is not None:
            dask_client.client = None  # this will start a local client
        else:
            client = get_dask_client(
                threads_per_worker=args.dask_nthreads,
                memory_limit=args.dask_memory,
            )
            dask_client.client = client
        dask_client.run(init_logging)

        for _, scheduler in dask_client.client.scheduler_info()["services"].items():
            print(f"Scheduler service port: {scheduler}")
        for workid, worker in dask_client.client.scheduler_info()["workers"].items():
            threads = worker["nthreads"]
            memory_limit = worker["memory_limit"]
            print(f"Worker {workid}: {threads} - {memory_limit}")

    else:
        dask_client.run(init_logging)

    # Set the muser array and fundametal parameters
    muser_array = args.muser.lower()
    file_list = collect_data_list(muser_array, args.sourcedir)

    if len(file_list) == 0:
        raise ("Cannot find any observational data file.")
    log.info("Found %d files in the directory %s", len(file_list), args.sourcedir)
    # selected_files = get_file_info_read_frame(file_list)
    selected_files = get_file_info_from_filename(file_list)
    log.info("Selected %d files for processing", len(selected_files))

    export_ms_files(dask_client, selected_files, args)

    log.info("Finished: %s", datetime.datetime.now())
    print("Finished: %s", datetime.datetime.now())
    if args.use_dask.lower == "true":
        dask_client.close()


if __name__ == "__main__":
    configure_iers_settings()
    main()
