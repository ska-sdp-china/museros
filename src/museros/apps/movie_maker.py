import argparse
import datetime
import logging
import os

import imageio

from museros.apps.apps_parser import apps_parser_app, apps_poarser_movie_maker_files

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)


def gen_movie():
    folder_path = "/path/to/your/images"

    image_files = [
        os.path.join(folder_path, filename)
        for filename in os.listdir(folder_path)
        if filename.endswith((".png", ".fits"))
    ]
    image_files.sort()

    with imageio.get_writer(
        "/path/to/output/animation.gif", mode="I", duration=0.5
    ) as writer:
        for filename in image_files:
            image = imageio.imread(filename)
            writer.append_data(image)

    print("DONE")


def main():
    """
    Main function for batch_exporter
    """
    parser = argparse.ArgumentParser(description="Read MUSER file information. ")
    parser = apps_parser_app(parser)
    parser = apps_poarser_movie_maker_files(parser)

    args = parser.parse_args()
