import argparse
import copy
import logging
import os
from copy import deepcopy

import numpy
from astropy import units as u

# from matplotlib import plt.savefig
from astropy.coordinates import (
    ITRS,
    AltAz,
    EarthLocation,
    SkyCoord,
    get_body,
    get_body_barycentric,
    get_moon,
    solar_system_ephemeris,
)
from astropy.time import Time
from astropy.wcs.utils import pixel_to_skycoord

# from rascil2.data_models.parameters import rascil2_path
from rascil2.data_models.polarisation import PolarisationFrame

# from rascil2.processing_components import sum_visibility
from rascil2.processing_components import (
    create_configuration_from_file,
    create_visibility,
)
from rascil2.processing_components.flagging.operations import flagging_visibility
from rascil2.processing_components.io.base import export_visibility_to_uvfits

from museros.apps.apps_parser import apps_parser_app, apps_parser_export_files
from museros.components.ephem.sun_position import get_sun, get_transit_time
from museros.components.utils.set_astropy_iers import configure_iers_settings
from museros.data_models.muser_calibration import MuserCalibration
from museros.data_models.muser_data import MuserData
from museros.data_models.muser_flag import MuserFlagging
from museros.data_models.parameters import (
    muser_data_path,
    muser_output_path,
    muser_path,
)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

log = logging.getLogger("muser-logger")

try:
    import casacore
    from casacore.tables import table  # pylint: disable=import-error
    from rascil2.processing_components.io.base import (
        create_visibility_from_ms,
        export_visibility_to_ms,
    )
    from rascil2.processing_components.visibility.base import create_visibility

    run_ms_tests = True
#            except ModuleNotFoundError:
except:
    run_ms_tests = False


def cli_parser():
    """Get a command line parser and populate it with arguments

    First a CLI argument parser is created. Each function call adds more arguments to the parser.

    :return: CLI parser argparse
    """

    parser = argparse.ArgumentParser(
        description="MUSER MS file exporter", fromfile_prefix_chars="@"
    )
    parser = apps_parser_app(parser)
    parser = apps_parser_export_files(parser)

    return parser


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def create_configuration(name: str = "muserl", **kwargs):
    conf_dir = muser_path("configurations")

    location = EarthLocation(
        lon=115.2505 * u.deg, lat=42.211833333 * u.deg, height=1365.0 * u.m
    )
    if name.lower() == "muserh":
        antfile = os.path.join(conf_dir, "muser-2.csv")
        lowcore = create_configuration_from_file(
            antfile=antfile,
            mount="altaz",
            names="I_%d",
            diameter=2.0,
            name="MUSERH",
            location=location,
            ecef=False,
            **kwargs,
        )
    elif name.lower() == "museri":
        antfile = os.path.join(conf_dir, "muser-1.csv")
        lowcore = create_configuration_from_file(
            antfile=antfile,
            mount="altaz",
            names="I_%d",
            diameter=4.5,
            name="MUSERI",
            location=location,
            ecef=False,
            **kwargs,
        )
    elif name.lower() == "muserl":
        antfile = os.path.join(conf_dir, "muser-low.csv")
        lowcore = create_configuration_from_file(
            antfile=antfile,
            mount="altaz",
            names="L%d",
            diameter=6.0,
            name="MUSERL",
            location=location,
            ecef=False,
            **kwargs,
        )
    elif name.lower() == "muserl-old":
        antfile = os.path.join(conf_dir, "muser-low-old.csv")
        lowcore = create_configuration_from_file(
            antfile=antfile,
            mount="altaz",
            names="L%d",
            diameter=6.0,
            name="MUSERL",
            location=location,
            ecef=False,
            **kwargs,
        )
    else:
        log.error("MUSER ARRAY: Array code error.")
    return lowcore


def muser_exporter(args):
    """_summary_

    :param args: arguments
    """
    if args.file is None:
        log.error("MS-Exporter: please input a xengine data file. Exit.")
        exit()

    # Initialize muser array
    muser_array = args.muser.lower()
    muser = MuserData(muser_array=muser_array, file_name=args.file)
    # Retrieve data information
    log.info(f"Checking MUSER File Information")
    log.info(f"First observational time:{muser.date_time}")
    # Check data
    log.info(f"Total number of frames:{muser.n_frames}")
    log.info(f"Band:{muser.band}")
    log.info(f"Banwidth:{muser.bandwidth}")
    log.info(f"Mode:{muser.mode}")
    log.info(f"Number of polarizations:{muser.n_polarizations}")
    log.info(f"Antennas:{muser.n_antennas}")

    if args.elevation == 0:
        elevation = None
    else:
        elevation = (args.elevation * u.deg).to("rad").value

    # Load Calibration data
    phase_cal = MuserCalibration(muser)
    if len(args.cal) > 0:
        log.info("Calibration file: " + args.cal)
        if not phase_cal.load_calibration_data(args.cal):
            raise "Cannot find phase calibration file. "
        log.info("Phase Calibration Data Loaded.")
        is_phase_calibration = True
    else:
        log.info("No calibration file specified..")

    flagging = MuserFlagging()
    log.info(
        f"Argument flag_baselines: {args.flag_baselines},{type(args.flag_baselines)}"
    )
    if len(args.flag_baselines) > 0:
        log.info("Flagging file: " + args.flag_baselines)
        if not flagging.load_flagged_baselines(args.flag_baselines):
            raise "Cannot find phase calibration file. "
        log.info("Baseline Data Flagged Loaded.")

    if args.flag_antennas is not None:
        # Read information of flagged antenna
        pass

    # # Create configuration of RASCIL
    muser_core = create_configuration(muser_array)

    freq = muser.generate_frequency_list()
    frequency = numpy.array(freq)
    log.info(f"Start Frequency: {frequency[0]}")
    log.info(f"Frequency shape is: {frequency.shape}")

    # integration_time = numpy.array([0.025])
    channel_bandwidth = numpy.array(
        [(125.0 / 128) * 1000000] * muser.n_channels * muser.n_bands
    )

    if args.nframes == 0:
        if muser.is_loop_mode:
            total_frames = muser.n_frames // 4
        else:
            total_frames = muser.n_frames
    else:
        total_frames = args.nframes
    log.info("Total {} frames will be processed.".format(total_frames))

    utc_time = Time(
        "%04d-%02d-%2dT00:00:00"
        % (
            muser.first_frame_utc_time.datetime.year,
            muser.first_frame_utc_time.datetime.month,
            muser.first_frame_utc_time.datetime.day,
        ),
        format="isot",
    )
    transit, _, _ = get_transit_time(utc_time.datetime)
    transit_time = Time(
        "%04d-%02d-%2dT%02d:%02d:%09.6f"
        % (
            muser.first_frame_utc_time.datetime.year,
            muser.first_frame_utc_time.datetime.month,
            muser.first_frame_utc_time.datetime.day,
            transit[0],
            transit[1],
            transit[2],
        ),
        format="isot",
    )
    log.info(f"Transit time of the Sun: {transit_time}")
    ref_time = float(
        Time(
            utc_time.datetime.strftime("%Y-%m-%dT00:00:00"),
            format="isot",
            scale="utc",
        ).unix
    )

    if muser.muser_array in ["muserh", "museri"]:
        if muser.is_loop_mode:
            vis_data = numpy.zeros(
                (
                    total_frames,
                    muser.n_antennas * (muser.n_antennas - 1) // 2 + muser.n_antennas,
                    muser.n_channels * muser.n_bands,
                    muser.n_polarizations,
                ),
                dtype=numpy.complex128,
            )
        else:
            vis_data = numpy.zeros(
                (
                    total_frames,
                    muser.n_antennas * (muser.n_antennas - 1) // 2 + muser.n_antennas,
                    muser.n_channels,
                    1,
                ),
                dtype=numpy.complex128,
            )
    else:
        if muser.is_loop_mode:
            vis_data = numpy.zeros(
                (
                    total_frames,
                    muser.n_antennas * (muser.n_antennas - 1) // 2 + muser.n_antennas,
                    muser.n_channels * muser.n_bands,
                    muser.n_polarizations,
                ),
                dtype=numpy.complex128,
            )
        else:
            vis_data = numpy.zeros(
                (
                    total_frames,
                    muser.n_antennas * (muser.n_antennas - 1) // 2 + muser.n_antennas,
                    muser.n_channels,
                    muser.n_polarizations,
                ),
                dtype=numpy.complex128,
            )

    log.info(f"VIS_Data shape is： {vis_data.shape}")
    times = []
    integration_time = []
    utc_times = []
    for count in range(total_frames):
        # Read one frame
        log.info("Load a frame from raw data")
        if not muser.read_frame():
            raise "Reading a frame error"
        # Retrieve visibilities
        if muser.muser_array in ["muserh", "museri"]:
            if muser.is_loop_mode:
                obs_time = muser.current_frame_utc_time + 0.0125 * u.second
            else:
                obs_time = muser.current_frame_utc_time + 0.0015625 * u.second
        else:
            obs_time = muser.current_frame_utc_time
            log.info(f"Observation frame time: {obs_time}")

        Alpha, Delta, ha, Thete_z, Phi = get_sun(obs_time.datetime)
        if ha > 180.0:
            ha = ha - 360.0
        log.info(
            f"Sun position: Alpha:{Alpha}, Delta:{Delta}，Ha:{ha}, {Thete_z}, {Phi}"
        )
        times.append([(ha * u.deg).to("rad").value])  # [local_ha.to('rad').value])
        log.info(f"Utc time: {obs_time}, MJD: {obs_time.mjd}")
        utc_times.append(obs_time)

        integration_time.append(0.1)

        phasecentre = SkyCoord(
            ra=Alpha * u.deg, dec=Delta * u.deg, frame="icrs", equinox="J2000"
        )

        # Load Phase Calibration Data
        log.info("Loading Phase Calibration File")
        if phase_cal.enabled:
            tmp_vis_data = phase_cal.phase_calibration(
                muser.vis_data, phase_cal.phase_data
            )
        else:
            tmp_vis_data = muser.vis_data[:, :, :]
        log.info(f"Count: {count}")
        if muser.muser_array in ["muserl"]:
            vis_data[count, :, :, :] = deepcopy(tmp_vis_data)
        else:
            if muser.is_loop_mode:
                vis_data[count, :, :, 1] = deepcopy(muser.vis_data[:, :, 0])
                vis_data[count, :, :, 0] = deepcopy(muser.vis_data[:, :, 1])
            else:
                vis_data[count, :, :, 0] = deepcopy(muser.vis_data[:, :, 0])

    times = numpy.array(times)

    integration_time = numpy.array(integration_time)
    log.info(f"Observational time {utc_time}")

    bvis = create_visibility(
        muser_core,
        times,
        frequency,
        phasecentre=phasecentre,
        weight=1.0,
        polarisation_frame=PolarisationFrame("linear"),
        channel_bandwidth=channel_bandwidth,
        integration_time=integration_time,
        source="SUN",
        elevation_limit=elevation,
        utc_time=utc_times,
    )

    if flagging.flag_baselines_enabled:
        log.info("Flagging visibilities ..")
        bvis = flagging_visibility(bvis, baselines=flagging.flag_baselines)

    bvis["vis"].data[...] = copy.deepcopy(vis_data)

    # old data format calibration
    if args.old_data.lower() == "true":
        muser_old_core = create_configuration("muserl-old")

        # TODO: prototype
        olduvwlamda = create_visibility(
            muser_old_core,
            times,
            frequency,
            phasecentre=phasecentre,
            weight=1.0,
            polarisation_frame=PolarisationFrame("linear"),
            channel_bandwidth=channel_bandwidth,
            integration_time=integration_time,
            source="SUN",
            elevation_limit=elevation,
            utc_time=utc_times,
        )

        delt_w_lamda = (
            bvis.uvw_lambda.data[..., 2] - olduvwlamda.uvw_lambda.data[..., 2]
        )
        delt_w_lamda.shape
        delt_w_phase_factor = numpy.exp(2.0 * numpy.pi * delt_w_lamda * 1j)
        delt_w_phase_factor[..., None].shape

        bvis.vis.data *= delt_w_phase_factor[..., None]

        indx = 0
        auto_index = []
        for base in bvis.baselines:
            if int(base.antenna1.data) == int(base.antenna2.data):
                # print(base.data)
                auto_index.append(indx)
            indx += 1

        conf_dir = muser_path("configurations")

        raw_Band1_phase_HH = []
        raw_Band1_phase_VV = []

        fillmask = numpy.ones(5151, dtype=bool)
        fillmask[auto_index] = False

        for force_band in range(4):

            new_phase_HH_band1 = numpy.zeros((5151, 104), dtype=float)
            new_phase_VV_band1 = numpy.zeros((5151, 104), dtype=float)

            Band1_phase_HH = numpy.loadtxt(
                os.path.join(conf_dir, f"MDRH_BLs_B{force_band+1}Phase_HH.txt")
            )
            Band1_phase_VV = numpy.loadtxt(
                os.path.join(conf_dir, f"MDRH_BLs_B{force_band+1}Phase_VV.txt")
            )

            new_phase_HH_band1[fillmask, :] = Band1_phase_HH.T
            new_phase_VV_band1[fillmask, :] = Band1_phase_VV.T

            raw_Band1_phase_HH.append(new_phase_HH_band1)
            raw_Band1_phase_VV.append(new_phase_VV_band1)

        raw_Band1_phase_HH = numpy.hstack(raw_Band1_phase_HH)
        raw_Band1_phase_VV = numpy.hstack(raw_Band1_phase_VV)

        raw_Band1_phase_HH = numpy.exp(raw_Band1_phase_HH * -1j)
        raw_Band1_phase_VV = numpy.exp(raw_Band1_phase_VV * -1j)

        bvis.vis.data[:, :, :, 0] *= raw_Band1_phase_HH
        bvis.vis.data[:, :, :, 1] *= raw_Band1_phase_VV

        flag_ant_HH = (
            [2, 11, 16, 17]
            + list(range(19, 26))
            + [27, 29, 48]
            + list(range(50, 61))
            + [69]
            + list(range(79, 90))
            + [93, 94, 101]
        )

        flag_ant_VV = (
            [2, 10, 11, 17]
            + list(range(19, 26))
            + [27, 29, 46]
            + list(range(50, 61))
            + [69, 72]
            + list(range(79, 90))
            + [93, 94, 101]
        )

        indx = 0
        flag_mask_HH = numpy.zeros(bvis.baselines.shape, dtype=bool)
        flag_mask_VV = numpy.zeros(bvis.baselines.shape, dtype=bool)
        for base in bvis.baselines:
            a1 = int(base.antenna1.data)
            a1 = int(base.antenna2.data)
            if (a1 in flag_ant_HH) or (a1 in flag_ant_HH):
                flag_mask_HH[indx] = True
            if (a1 in flag_ant_VV) or (a1 in flag_ant_VV):
                flag_mask_VV[indx] = True
            indx += 1

        bvis.vis.data[:, flag_mask_HH, :, 0] = 0.0
        bvis.vis.data[:, flag_mask_VV, :, 3] = 0.0

    vis_list = []
    vis_list.append(bvis)

    # Output results
    if len(args.output) == 0:
        output_time = muser.first_date_time
        file_name = "MUSER%04d%02d%02d-%02d%02d%02d" % (
            output_time.year,
            output_time.month,
            output_time.day,
            output_time.hour,
            output_time.minute,
            output_time.second,
        )
        if args.type.lower() == "ms":
            export_file_name = muser_output_path(file_name) + ".ms"  # data_file_name
        else:
            export_file_name = muser_output_path(file_name) + ".fits"  # data_file_name
    else:
        export_file_name = muser_output_path(args.output)
    if args.type.lower() == "ms":
        export_visibility_to_ms(
            export_file_name, vis_list, source_name="SUN", nband=muser.n_bands
        )
    else:
        export_visibility_to_uvfits(
            export_file_name, vis_list, source_name="SUN", ref_time=ref_time
        )
    log.info("Export file: {}".format(export_file_name))
    log.info("Done. ")


def main():
    log.info("Starting exporting meansurement set file")

    parser = cli_parser()
    args = parser.parse_args()

    if args.logfile is not None:
        logging.basicConfig(
            format="%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s",
            level=logging.DEBUG,
        )

        fh = logging.FileHandler(args.log, mode="w")
        fh.setLevel(logging.DEBUG)
        log.addHandler(fh)
    else:
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        log.addHandler(console)
        log.setLevel(level=logging.INFO)
        log.info("Logging device: CONSOLE")

    if args.loglevel is not None:
        if args.loglevel == "INFO":
            log.setLevel(level=logging.INFO)
        elif args.loglevel == "DEBUG":
            log.setLevel(level=logging.DEBUG)
        elif args.loglevel == "WARNING":
            log.setLevel(level=logging.WARNING)
        elif args.loglevel == "ERROR":
            log.setLevel(level=logging.ERROR)
        else:
            log.setLevel(level=logging.INFO)

    muser_exporter(args)


if __name__ == "__main__":
    configure_iers_settings()
    main()
