"""
  Data model for MUSER radioheliograph
"""

__all__ = ["MuserData"]

import logging

import numpy
from astropy import units as u
from astropy.coordinates import EarthLocation, SkyCoord, solar_system_ephemeris
from astropy.time import Time

from museros.components.ephem.sun_position import get_sun
from museros.components.io.xengine_muser import MuserXEngine
from museros.data_models.parameters import muser_data_path, muser_path

log = logging.getLogger("muser-logger")


class MuserData(MuserXEngine):
    def __init__(self, muser_array=None, file_name=None):
        """
        Constructor of MuserData

        :param sub_array: Muser array ('MUSERL'/'MUSERI'/'MUSERH'), defaults to 'MUSERI'
        :param file_name: Raw data file name, defaults to None
        :param cal_file: Calibration file (Python npy format)
        """

        # Initialize the parent class constructor function
        super(MuserData, self).__init__(muser_array=muser_array, file_name=file_name)

    def generate_frequency_list(self):
        """Generate frequency list

        :param muser MuserData object

        :return Frequency list
        """
        freq = []
        if self.muser_array in ["muserl", "museri"]:
            # MUSER Low
            if self.mode == "LOOP":
                BandAvg = self.frame.frame_head.bandwidth
                for BandInx in range(1, 5):
                    BdAvgInc = BandAvg / 2.0 - 0.5

                    if BandInx == 1:
                        Frequency = (15 + BdAvgInc) * 125 / 128
                    else:
                        Frequency = (
                            (15 + BdAvgInc) * 125 / 128 + (BandInx - 2) * 100 + 85
                        )
                    Frequency *= 1e6
                    chan_band = 125 * BandAvg / 128
                    chan_band *= 1e6
                    frequency = numpy.arange(self.n_channels) * chan_band + Frequency
                    freq = freq + frequency.tolist()
            else:
                BandInx = self.band
                BandAvg = self.bandwidth
                BdAvgInc = BandAvg / 2.0 - 0.5

                if BandInx == 1:
                    Frequency = (15 + BdAvgInc) * 125 / 128
                else:
                    Frequency = (15 + BdAvgInc) * 125 / 128 + (BandInx - 2) * 100 + 85
                chan_band = 125 * BandAvg / 128
                Frequency *= 1e6
                chan_band *= 1e6
                frequency = numpy.arange(self.n_channels) * chan_band + Frequency
                freq = freq + frequency.tolist()
        return freq

    def read_file(self, nframes=0):
        """
        Read a full frame
        :param nframe: the number of frames

        :return: status （True/False)
        """

        self.integration_time = []
        self.times = []
        count = 0
        while count < self.n_frames:
            if not self.read_frame():
                log.error("File reading error. ")
                exit(1)

            obs_time = Time(self.date_time, scale="utc")
            log.debug(f"No.{count} : Observation time (UTC): {obs_time}")

            Alpha, Delta, ha, Thete_z, Phi = get_sun(self.date_time)
            if ha > 180.0:
                ha = ha - 360.0
            log.debug(
                f"Sun position: Alpha:{Alpha}, Delta:{Delta}，Ha:{ha}, {Thete_z}, {Phi}"
            )
            self.times.append((ha * u.deg).to("rad").value)
            self.integration_time.append(3.0125)

            # Inject data into visibility
            if self.muser_array == "muserl":
                self.vis_data[count, :, :, :] = self.vis_data[:, :, :]
            elif self.muser_array == "museri":
                self.vis_data[count, :, :, :] = self.vis_data[:, :, :]

            count = count + 1

        return True

    # def delay_process(self, planet):
    #     # print "delay processing..."
    #     parameter = 0.0
    #     delay = numpy.ndarray(shape=(self.dr_output_antennas), dtype=float)

    #     log.debug("Block Data Delay Process and fringe stopping... Done.")
