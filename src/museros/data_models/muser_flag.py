""" Phase Calibration

"""

__all__ = ["MuserFlagging"]

import logging
import os

import numpy
from astropy.time import Time

from museros.data_models.parameters import (
    muser_calibration_path,
    muser_data_path,
    muser_output_path,
    muser_path,
)

log = logging.getLogger("muser")


class MuserFlagging:
    def __init__(
        self,
    ):
        """Constructor
        :param muser Muser Array
        """
        self.flag_baselines = []
        self.flag_baselines_enabled = False
        self.flag_antennas = []
        self.flag_antennas_enabled = False

    def flagging_antennas(self, filename):
        """Flagging Vis using antenna pairs
        :param filename: Filename

        :return A list of antenna
        """
        pass

    def load_flagged_baselines(self, filename):
        """Flagging Vis using baselines

        :param filename: Baseline Flagging file
        :type filename: str
        :return: A list of baseline flagged
        """
        import pandas as pd

        log.info(f"Flagging baseline file name: {filename}")
        fd = pd.read_csv(filename)
        self.flag_baselines = list(fd[fd["flag"] == 0].index)
        self.flag_baselines_enabled = True
        return True
