""" Phase Calibration

"""

__all__ = ["MuserCalibration"]

import logging
import os

import numpy
from astropy.time import Time

from museros.data_models.muser_data import MuserData
from museros.data_models.parameters import (
    muser_calibration_path,
    muser_data_path,
    muser_output_path,
    muser_path,
)

log = logging.getLogger("muser")


class MuserCalibration:
    def __init__(
        self,
        muser,
    ):
        """Constructor
        :param muser Muser Array
        """
        if not isinstance(muser, MuserData):
            raise "Muser Phase Calibration need a MuserData object."
        self.muser = muser
        self.enabled = False

    def load_calibration_data(self, file_name=""):
        """Load calibration data from the file

        :param file_name: calibration file name

        :return True/False
        """
        if os.path.isfile(file_name):
            self.phase_data = numpy.fromfile(file_name, dtype=numpy.float64)
            self.phase_data = self.phase_data.reshape(
                self.muser.n_baselines,
                self.muser.n_channels * self.muser.n_bands,
                self.muser.n_polarizations,
            )
            log.info("Load Calibrated data.")
            self.enabled = True
        else:
            log.info("Ignore calibrated data.")
        return True

    def phase_calibration(self, vis_data, phai_cal=None):
        """Make phase calibration

        :param cal
        :param phai_sat
        """

        log.debug("Calibration phase calibration")
        amplitude = abs(vis_data)
        phai_sun = numpy.arctan2(vis_data.imag, vis_data.real)
        phai = phai_sun - phai_cal
        real = amplitude * numpy.cos(phai)
        imag = amplitude * numpy.sin(phai)
        return numpy.vectorize(complex)(real, imag)

    def flagging_baselines(filename):
        """Flagging Vis using baselines

        :param filename: Baseline Flagging file
        :type filename: str
        :return: A list of baseline flagged
        """
        import pandas as pd

        fd = pd.read_csv(filename)
        return list(fd[fd["flag"] == 0].index)
