# Data receiver for Xengine

import logging
import os
import sys
from struct import unpack

import numpy

log = logging.getLogger("muser-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


class XengineMuserIFrameHeader:
    """
    Frame of XEngine Used in MUSER
    """

    def __init__(self):
        """
        Constructor of XengineFrameHeader
        """
        self.year = None
        self.month = None
        self.day = None
        self.hour = None
        self.minute = None
        self.second = None
        self.milli_second = None
        self.micro_second = None
        self.polarisation = None
        self.channel = None
        self.delay_switch = None
        self.phase_switch = None
        self.source = None
        self.bandwidth = None
        self.period_number = None
        self.integrate_period = None

    def __repr__(self):
        return f'Frame:("{self.year}","{self.month}","{self.day}","{self.hour}","{self.minute}","{self.second}", "{self.milli_second}","{self.micro_second}","{self.polarisation}","{self.channel}")'

    def __str__(self):
        return f"Frame: {self.year}-{self.month:02d}-{self.day:02d} {self.hour:02d}:{self.minute:02d}:{self.second:02d}.{self.milli_second}{self.micro_second} - Pol:{self.polarisation:X}, Channel:{self.channel:X}, Bandwidth:{self.bandwidth}"


class XengineMuserIFrame:
    def __init__(self, xengine_file=None, muser_array="LOW", bandwidth=16):
        """Constructor of XengineFrame

        :param xengine_file: _description_, defaults to None
        :param muser_array: _description_, defaults to "LOW"
        :param bandwidth: _description_, defaults to 16
        """

        # File handler
        self.xengine_file = xengine_file
        # Frame header
        self.frame_head = XengineMuserIFrameHeader()
        # Frame data
        self.number_of_frame_dict = {1: 400, 4: 100, 16: 25}
        self.n_channels = self.number_of_frame_dict[bandwidth]
        self.n_antennas = 42
        if muser_array == "HIGH":  # Muser Low Array
            self.n_antennas = 60  ## TODO

        self.auto_correlation = numpy.ndarray(
            (self.n_antennas, self.n_channels), dtype=numpy.uint32
        )
        self.auto_correlation_square = numpy.ndarray(
            (self.n_antennas, self.n_channels), dtype=numpy.uint64
        )
        self.cross_correlation = numpy.ndarray(
            (self.n_antennas, self.n_antennas, self.n_channels), dtype=numpy.uint32
        )

    def read_frame_head(self):
        """Read XEngine Frame

        :return: Reading result (True/False)
        """
        if self.xengine_file is None:
            return False
        buff = self.xengine_file.read(20)
        (
            self.frame_head.year,
            self.frame_head.month,
            self.frame_head.day,
            self.frame_head.hour,
            self.frame_head.minute,
            self.frame_head.second,
            self.frame_head.milli_second,
            self.frame_head.micro_second,
            self.frame_head.polarisation,
            self.frame_head.channel,
            self.frame_head.delay_switch,
            self.frame_head.phase_switch,
            self.frame_head.source,
            self.frame_head.bandwidth,
            self.frame_head.period_number,
            self.frame_head.integrate_period,
        ) = unpack("<HBBBBBHHBBBBBBHB", buff)

        log.info(f"File pointer after header {self.xengine_file.tell()}")

        return True

    def read_frame_p0_p6(self):
        """
        Read Auto-Correlation From Frame type 1 (P0-P6)

        :return: Reading result (True/False)
        """
        # p0 - p6
        log.info(f"File pointer before p0-6 {self.xengine_file.tell()}")
        log.info(f"Channels={self.n_channels}")
        for p in range(7):
            for channel in range(self.n_channels):
                # log.info(f"Reading Type-1 frame: {p:2d} - {channel:2d}.")
                # Read RiRi->Ri+5Ri+5 auto_correlation
                buff = self.xengine_file.read(24)
                self.auto_correlation[p * 6 : p * 6 + 6, channel] = unpack(
                    ">iiiiii", buff
                )
                # Read RiRi->Ri+5Ri+5 auto_correlation_square
                buff = self.xengine_file.read(48)
                self.auto_correlation_square[p * 6 : p * 6 + 6, channel] = unpack(
                    ">qqqqqq", buff
                )
        log.info(f"File pointer after p0-6 {self.xengine_file.tell()}")

        return True

    def read_frame_p7_p13(self):
        """
        Read CrossCorrelation Frame type 2 (P7-P13)

        :return: Reading result (True/False)
        """
        # p7 - p13
        for p in range(7):
            for channel in range(self.n_channels):
                # log.info(f"Reading Type-2 frame: {p:2d} - {channel:2d}.")
                # Read RiRi->Ri+5Ri+5 auto_correlation
                buff = self.xengine_file.read(120)
                format = ">" + "i" * 30
                result = unpack(format, buff)
                correlation_tmp = numpy.array(result, dtype=numpy.int32)
                correlation = correlation_tmp[0::2] + 1j * correlation_tmp[1::2]
                index = 0
                for antenna1 in range(p * 6, p * 6 + 5):
                    for antenna2 in range(antenna1 + 1, p * 6 + 6):
                        self.cross_correlation[antenna1, antenna2, channel] = (
                            correlation[index]
                        )
                        index += 1
        log.info(f"File pointer after p7-13 {self.xengine_file.tell()}")
        return True

    def read_frame_p14_p34(self):
        """
        Read CrossCorrelation Frame type 3 (P14-P34)

        :return: Reading result (True/False)
        """
        # p14 - p34
        frame_order = (
            (0, 1),
            (0, 2),
            (0, 3),
            (0, 4),
            (0, 5),
            (0, 6),
            (5, 6),
            (1, 2),
            (1, 3),
            (1, 4),
            (1, 5),
            (1, 6),
            (4, 5),
            (4, 6),
            (2, 3),
            (2, 4),
            (2, 5),
            (1, 6),
            (3, 4),
            (3, 5),
            (3, 6),
        )
        for bstart, bend in frame_order:
            for channel in range(self.n_channels):
                # log.info(f"Reading Type-3 frame: B{bstart}-B{bend}.")

                buff = self.xengine_file.read(288)
                format = ">" + "i" * 72
                result = unpack(format, buff)
                correlation_tmp = numpy.array(result, dtype=numpy.int32)
                correlation = correlation_tmp[0::2] + 1j * correlation_tmp[1::2]
                index = 0
                for antenna1 in range(6):
                    for antenna2 in range(6):
                        self.cross_correlation[
                            bstart * 6 + antenna1, bend * 6 + antenna2, channel
                        ] = correlation[index]
                        index += 1

        return True


class Xengine:
    def __init__(self, filename=None, muser_array=0):
        """
        Constructor of Xenging

        :param filename: File name

        """
        self.file_name = filename
        self.xengine_file = None
        self.number_of_frame = None
        self.antenna_number = [42]
        self.frame = XengineMuserIFrame()

    def open(self, filename=None):
        """
        Open the
        :return:
        """
        if filename is not None:
            self.file_name = filename
        if os.path.exists(self.file_name):
            self.frame.xengine_file = open(self.file_name, "rb")
            log.info("Open file successfully.")
            return True
        else:
            log.error("Cannot open the file.")
        self.xengine_file = None
        return False

    def read_frame(self):
        """Read one frame"""
        # Get info from frame header
        if not self.frame.read_frame_head():
            log.error("Reading frame header error.")
        # Read one frame
        if not self.frame.read_frame_p0_p6():
            log.error("Reading frame (p0-p6) error.")
        if not self.frame.read_frame_p7_p13():
            log.error("Reading frame (p7-p13) error.")
        if not self.frame.read_frame_p14_p34():
            log.error("Reading frame (p14-p34) error.")
        log.info(f"Current file pointer {self.frame.xengine_file.tell()}")


if __name__ == "__main__":
    file_name = "pkt_src1_off_W16_I9600_0"
    xfile = Xengine(muser_array="LOW")
    if not xfile.open(file_name):
        print("Error: No files exist.")
        exit(-1)
    xfile.read_frame()
