.. _muser__data_models:

.. py:currentmodule:: museros.data_models

Data Models
**********

.. toctree::
   :maxdepth: 3

.. automodapi::    museros.data_models.muser_data
   :no-inheritance-diagram:

.. automodapi::    museros.data_models.muser_calibration
   :no-inheritance-diagram:

.. automodapi::    museros.data_models.muser_flag
   :no-inheritance-diagram:

.. automodapi::    museros.data_models.parameters
   :no-inheritance-diagram:
