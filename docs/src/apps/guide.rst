.. app-guide:

*****************
MUSEROS Applications
*****************

These scripts process the raw observational data of Mingantu Radioheliograph.

    - Read each frame from raw data of MUSER.
    - Generate phase calibration file.
    - Export Measurement-set file (MS).
    - Export UVFITS files.
    - Web framework for providing UI for endusers.

The fits images can be viewed using the casaviewer or carta. The MeasurementSets can be viewed
using casaviewer.

If you are running on your own machine, make sure you have the below environment variables set up:

    SMUSER : location where the museros repository is.
    SMUSER_DATA : location of the resources e.g. configuration files.

Data Reader
=================

- Read XEngine format.
- Read header of each frame
- Will suporrt other digital-correlator.

Measurement-set and Unfits File Exporter
#####################

- Read XEngien data.
- Generate Meansurement set file.
- Calculate the observational time and UVW.
- Support MuerL, MuserI and MuserH

.. argparse::
   :module: muser
   :func: cli_parser
   :prog: muser_export.py

Get Information From Raw file
#####################

.. argparse::
   :module: muser
   :func: cli_parser
   :prog: get_file_info.py