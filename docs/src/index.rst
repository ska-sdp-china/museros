.. _documentation_master:

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

MUSER OS V2.0
===========

MUSEROS is a data acquisition, processing, and archiving system developed specifically for
the Mingantu Radioheliograph (MUSER).
This package collects scripts inclduing: xEngine data reader, real time imaging, Meansurement set file writer,
and UVFITS write.

The package uses `MUSEROS <https://gitlab.com/ska-sdp-china/museros.git>` as its official repository.

.. toctree::
  :maxdepth: 1

  apps/guide
  components/index

Instructions
============

To install these scripts, use git::

    git clone https://gitlab.com/ska-sdp-china/museros.git


Or download the source code from https://https://gitlab.com/ska-sdp-china/museros

None of these simulations need to be installed into the python path. Instead please set the below environment variables::

    export SMUSEROS=/path/to/code
    export SMUSER_DATA=/path/to/results


RASCIL2 must be installed - the options are via pip, git clone, or docker. The simplest approach is to use pip. In the
museros directory, do::

    pip3 install -r requirements.txt

A script can be run as::

    export SMUSEROS=`pwd`
    cd sizing/scripts
    sh make_sizes.sh

The configuration files for the MUSER I/L/H can be found under museros directory.